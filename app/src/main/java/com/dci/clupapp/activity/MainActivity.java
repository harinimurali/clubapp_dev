package com.dci.clupapp.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.dci.clupapp.R;

import com.dci.clupapp.adapter.DrawerAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.fragment.AboutFragment;
import com.dci.clupapp.fragment.ChatFragment;
import com.dci.clupapp.fragment.ConnectFragment;
import com.dci.clupapp.fragment.ContactFragment;
import com.dci.clupapp.fragment.EventFragment;
import com.dci.clupapp.fragment.GalleryFragment;
import com.dci.clupapp.fragment.HomeFragment;
import com.dci.clupapp.fragment.MemberlistFragment;
import com.dci.clupapp.fragment.MusicPlayerFragment;
import com.dci.clupapp.fragment.NewsFragment;
import com.dci.clupapp.fragment.PostFragment;
import com.dci.clupapp.fragment.ProjectListFragment;
import com.dci.clupapp.fragment.VideoFragment;
import com.dci.clupapp.fragment.VideoViewFragment;
import com.dci.clupapp.fragment.WebsiteBlogFragment;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.github.fabtransitionactivity.SheetLayout;
import com.sergiocasero.revealfab.RevealFAB;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity implements SheetLayout.OnFabAnimationEndListener, AHBottomNavigation.OnTabSelectedListener {
    private RevealFAB revealFAB;
    /* @BindView(R.id.bottom_sheet)
     SheetLayout mSheetLayout;*/
    /*@BindView(R.id.menu)
    ImageView menu;*/
    //  @BindView(R.id.title)
    public static TextView title;
    @BindView(R.id.bottom_navigation)
    AHBottomNavigation ahBottomNavigation;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Fragment fragment;
    @BindView(R.id.notification)
    ImageView notification;
    @BindView(R.id.list_view)
    ListView listView;
    @BindView(R.id.close)
    ImageView closeMenu;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.username)
    TextView userName;
    String[] sidemenu;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;

    private static final int REQUEST_CODE = 1;
    private CharSequence mTitle;
    private ActionBarDrawerToggle drawerToggle;
    CommonResponse commonResponse;
    boolean doubleBackToExitPressedOnce = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_side_menu);
        // revealFAB =  findViewById(R.id.reveal_fab);
        //     mSheetLayout = findViewById(R.id.bottom_sheet);
        title = findViewById(R.id.title);
        ButterKnife.bind(this);
        createitems();
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        fcmSharedPrefrences = getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        //   mSheetLayout.setFab(menu);
        //  mSheetLayout.setFabAnimationEndListener(this);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setSubtitleTextColor(Color.WHITE);
        ahBottomNavigation.setOnTabSelectedListener(this);
        ahBottomNavigation.setAccentColor(getResources().getColor(R.color.colorPrimary));
        sidemenu = getResources().getStringArray(R.array.system);
        // revealFAB.setIntent(new Intent(MainActivity.this, Sample.class));

       /* revealFAB.setOnClickListener(new RevealFAB.OnClickListener() {
            @Override
            public void onClick(RevealFAB button, View v) {
                button.startActivityWithAnimation();
            }
        });*/
        try {
            Intent i = getIntent();
            String type = i.getStringExtra("type");
            if (type.equals("chat")) {

                fragment = new ChatFragment();
                replaceFragment(fragment);
                title.setText(getResources().getString(R.string.chat));
                setTitle(getResources().getString(R.string.chat));
            } else {
                fragment = new HomeFragment();
                replaceFragment(fragment);
                setTitle("Welcome " + AppPreferences.getProfile(getApplication()).get(0).getResults().getFirstname());
            }
        } catch (Exception e) {
            e.printStackTrace();
            fragment = new HomeFragment();
            replaceFragment(fragment);
            setTitle("Welcome " + AppPreferences.getProfile(getApplication()).get(0).getResults().getFirstname());

        }
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                startActivity(intent);
            }
        });

        userName.setText("Welcome " + AppPreferences.getProfile(getApplication()).get(0).getResults().getFirstname());
        //  setTitle("Welcome " + AppPreferences.getProfile(getApplication()).get(0).getResults().getFirstname());
       /* menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSheetLayout.expandFab();
            }
        });*/
        //  getSupportActionBar().setDisplayShowHomeEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.app_name, R.string.app_name);
        drawer.setDrawerListener(drawerToggle);

        //  mDrawerList = (ListView) findViewById(R.id.navdrawer);
        DrawerAdapter adapter1 = new DrawerAdapter(MainActivity.this, sidemenu);
        listView.setAdapter(adapter1);

        closeMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawers();
            }
        });
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                android.app.FragmentManager fragmentManager = getFragmentManager();
                switch (position) {
                    case 0:
                        drawer.closeDrawers();
                        Fragment fragment = new MemberlistFragment();
                        replaceFragment(fragment);
                        //setTitle("Edukool Parent");
                        title.setText(getResources().getString(R.string.member_directory));
                        break;

                    case 1:
                        drawer.closeDrawers();
                        Fragment fragment1 = new EventFragment();
                        replaceFragment(fragment1);
                        //setTitle("Attendance");
                        title.setText(getResources().getString(R.string.events));

                        break;
                    case 2:
                        drawer.closeDrawers();
                        Fragment fragment2 = new ContactFragment();
                        replaceFragment(fragment2);
                        // setTitle("Assignment");
                        title.setText(getResources().getString(R.string.contact_us));
                        break;
                    case 3:
                        drawer.closeDrawers();
                        Fragment result = new AboutFragment();
                        replaceFragment(result);
                        //toolbar.setTitle("Results");
                        title.setText(getResources().getString(R.string.about));
                        break;
                    case 4:
                        drawer.closeDrawers();
                        Fragment pay = new GalleryFragment();
                        replaceFragment(pay);
                        title.setText(getResources().getString(R.string.gallery));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;

                    case 5:
                        drawer.closeDrawers();
                        Fragment chat = new ChatFragment();
                        replaceFragment(chat);
                        title.setText(getResources().getString(R.string.chat));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 6:
                        drawer.closeDrawers();
                        Fragment leave = new VideoViewFragment();
                        replaceFragment(leave);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.video));
                        break;
                    case 7:
                        drawer.closeDrawers();
                        Fragment tt = new ConnectFragment();
                        replaceFragment(tt);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.connect));
                        break;
                    case 8:
                        drawer.closeDrawers();
                        Fragment exam = new ProjectListFragment();
                        replaceFragment(exam);
                        title.setText(getResources().getString(R.string.project_list));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 9:
                        drawer.closeDrawers();
                        Fragment fragmentt = new NewsFragment();
                        replaceFragment(fragmentt);
                        // setTitle("Recent Activities");
                        title.setText(getResources().getString(R.string.news));
                        break;

                    case 10:
                        drawer.closeDrawers();
                        Fragment news = new MusicPlayerFragment();
                        replaceFragment(news);
                        // setTitle("Pay Fees");
                        title.setText(getResources().getString(R.string.music));
                        break;
                    case 11:
                        drawer.closeDrawers();
                        Fragment pol = new WebsiteBlogFragment();
                        replaceFragment(pol);
                        title.setText(getResources().getString(R.string.blog));
                        // setTitle("Pay Fees");
                        // title.setText("Language");
                        break;
                    case 12:
                        drawer.closeDrawers();
                        Fragment circle = new PostFragment();
                        replaceFragment(circle);
                        // toolbar.setTitle("Circulars");
                        title.setText(getResources().getString(R.string.post));
                        break;
                    case 13:
                        drawer.closeDrawers();
                        logoutDialog();
                        break;

                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        // revealFAB.onResume();
    }

    @Override
    public void onFabAnimationEnd() {
        Intent intent = new Intent(this, SidemenuActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestcode", "onActivityResult: requestcode" + requestCode);
        Log.e("resultcode", "onActivityResult: " + resultCode);

        Log.e("data", "onActivityResult: " + data);

/*
        if (requestCode == REQUEST_CODE) {
            try {
                String returnValue = data.getStringExtra("type");
                if (returnValue.equals("member")) {
                    fragment = new MemberlistFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.member_directory));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("event")) {
                    fragment = new EventFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.events));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("contactus")) {
                    fragment = new ContactFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.contact_us));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("about")) {
                    fragment = new AboutFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.about));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("gallery")) {
                    fragment = new GalleryFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.gallery));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("projects")) {
                    fragment = new ProjectListFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.project_list));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("connect")) {
                    fragment = new ConnectFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.connect));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("video")) {
                    fragment = new VideoFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.video));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("news")) {
                    fragment = new NewsFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.news));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("music")) {
                    fragment = new MusicPlayerFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.music));
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("post")) {
                    fragment = new PostFragment();
                    replaceFragment(fragment);
                    setTitle("Posts");
                    mSheetLayout.contractFab();
                } else if (returnValue.equals("blog")) {
                    fragment = new WebsiteBlogFragment();
                    replaceFragment(fragment);
                    setTitle(getResources().getString(R.string.blog));
                    mSheetLayout.contractFab();
                }
            } catch (Exception e) {
                e.printStackTrace();
                mSheetLayout.contractFab();
            }
        }
*/
    }

    private void createitems() {

        AHBottomNavigationItem homeitem = new AHBottomNavigationItem(getString(R.string.home), R.drawable.ic_dashboard_home);
        AHBottomNavigationItem theatreitem = new AHBottomNavigationItem(getString(R.string.member_directory), R.drawable.ic_dashboard_memeber);
        AHBottomNavigationItem searchitem = new AHBottomNavigationItem(getString(R.string.gallery), R.drawable.ic_dashboard_gallery);
        ahBottomNavigation.addItem(homeitem);
        ahBottomNavigation.addItem(theatreitem);
        ahBottomNavigation.addItem(searchitem);
        ahBottomNavigation.setCurrentItem(0);
        ahBottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        ahBottomNavigation.setTranslucentNavigationEnabled(true);

     /*  setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
               getSupportActionBar().setTitle("Club App");
        }*/
    }

    public static void setTitle(String msg) {
        title.setText(msg);
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        if (position == 0) {
            fragment = new HomeFragment();
            replaceFragment(fragment);
            setTitle("Welcome " + AppPreferences.getProfile(getApplication()).get(0).getResults().getFirstname());
        } else if (position == 1) {
            fragment = new MemberlistFragment();
            replaceFragment(fragment);
            setTitle(getResources().getString(R.string.member_directory));
        } else if (position == 2) {
            fragment = new GalleryFragment();
            replaceFragment(fragment);
            setTitle(getResources().getString(R.string.gallery));
        }
        return true;
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;


        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    public void logoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(R.string.confrim);
        builder.setMessage(R.string.logout_string)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        CallApiLogout();
                       /* List<ChildrenProfile> ch = new ArrayList<>();
                        ChildrenProfile p = new ChildrenProfile();
                        AppPreferences.setParentNumber(MainActivity.this, "");
                        AppPreferences.setLanguage(MainActivity.this, 0);
                        AppPreferences.setParentName(MainActivity.this, "");
                        AppPreferences.saveLoginData(MainActivity.this, false, "", "", "", "", "", "", "", "");
                        AppPreferences.setchildrenProfile(MainActivity.this, ch);
                        AppPreferences.setParentImgae(MainActivity.this, "");
                        startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));*/

                        // FIRE ZE MISSILES!
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void CallApiLogout() {
        String userid = sharedPreferences.getString(Constants.USERID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String device_imei = String.valueOf(sharedPreferences.getInt(Constants.DEVICEIMEI, 0));
        String device_name = sharedPreferences.getString(Constants.DEVICENAME, "");

        clubAPI.logout(userid, fcm_key, deviceid, deviceos, appversion, device_imei, device_name).
                enqueue(new Callback<CommonResponse>() {


                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        try {
                            hideProgress();
                            if (response.body() != null) {
                                commonResponse = response.body();
                                Log.e("response", ">>" + response.body());
                                if (commonResponse.getStatus().equals("Success")) {

                                    Toast.makeText(MainActivity.this, "Logout Successfully", Toast.LENGTH_SHORT).show();
                                    AppPreferences.setLoginStatus(MainActivity.this, false);
                                    //  AppPreferences.setProfile(MainActivity.this,"");
                                    editor.putString(Constants.USERID, "").commit();
                                    editor.putString(Constants.VENDORID, "").commit();
                                    editor.putBoolean(Constants.LOGIN_STATUS, false).commit();

                                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);


                                }


                            }
                        } catch (Exception e) {
                            //  showProgress();
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Try again..", Toast.LENGTH_SHORT).show();
                        }

                    }


                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(MainActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {


            Log.e("back_arrow pressed", "back_arrow pressed");

            FragmentManager manager = getSupportFragmentManager();

            Log.e("manager", "entrycount" + manager.getBackStackEntryCount());

            if (manager.getBackStackEntryCount() == 1) {

           /* // Toast.makeText(MainActivity.this, "skjfghgsdfhjdsf", Toast.LENGTH_SHORT).show();
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getBaseContext(), "Tap back_arrow button in order to exit", Toast.LENGTH_SHORT).show();
            }*/

                //  mBackPressed = System.currentTimeMillis();
                String backStateName;
                backStateName = ((Object) new HomeFragment()).getClass().getName();
                String fragmentTag = backStateName;

                FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.main, new HomeFragment(), fragmentTag);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(backStateName);
                ft.commit();

                if (doubleBackToExitPressedOnce) {
                    finish();
                }

                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
            super.onBackPressed();
        }

    }

}