package com.dci.clupapp.activity;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dci.clupapp.R;

import java.io.IOException;

public class MediaPlayerActivity extends AppCompatActivity {
    Button buttonStop, buttonStart;
    private TextView elapsedTimeLabel;

    String AudioURL = "https://www.android-examples.com/wp-content/uploads/2016/04/Thunder-rumble.mp3";
    private static int oTime =0, sTime =0, eTime =0, fTime = 5000, bTime = 5000;

    String p;
    MediaPlayer mediaplayer;
    ImageView play_icon,pause_icon;
    private SeekBar seekbar;
    private Handler hdlr = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_player);

        pause_icon=findViewById(R.id.pause_icon);

        play_icon=findViewById(R.id.play_icon);
        seekbar=findViewById(R.id.seekbar);
        elapsedTimeLabel=findViewById(R.id.elapsedTimeLabel);


        seekbar.setClickable(false);

        mediaplayer = new MediaPlayer();
        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        play_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                try {

                    mediaplayer.setDataSource(AudioURL);
                    mediaplayer.prepare();


                } catch (IllegalArgumentException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                mediaplayer.start();

                if(oTime == 0){
                    seekbar.setMax(eTime);
                    oTime =1;
                }

                mRunnable.run();
                // play_icon.setImageResource(R.mipmap.pause);

                //  play_icon.setImageResource(R.mipmap.pause);



            }
        });


        pause_icon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                mediaplayer.stop();


            }
        });
    }




    public Handler mHandler = new Handler();

    public Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            if(mediaplayer != null) {

                //set max value
                int mDuration = mediaplayer.getDuration();
                seekbar.setMax(mDuration);

                //update total time text view
                TextView remainingTimeLabel = (TextView) findViewById(R.id.remainingTimeLabel);
                remainingTimeLabel.setText(getTimeString(mDuration));

                //set progress to current position
                int mCurrentPosition = mediaplayer.getCurrentPosition();
                seekbar.setProgress(mCurrentPosition);

                //update current time text view
                TextView elapsedTimeLabel = (TextView) findViewById(R.id.elapsedTimeLabel);
                elapsedTimeLabel.setText(getTimeString(mCurrentPosition));
                //handle drag on seekbar
                seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if(mediaplayer != null && fromUser){
                            mediaplayer.seekTo(progress);
                        }
                    }
                });


            }

            //repeat above code every second
            mHandler.postDelayed(this, 10);
        }
    };

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        long hours = millis / (1000*60*60);
        long minutes = ( millis % (1000*60*60) ) / (1000*60);
        long seconds = ( ( millis % (1000*60*60) ) % (1000*60) ) / 1000;

        buf
                .append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

}

