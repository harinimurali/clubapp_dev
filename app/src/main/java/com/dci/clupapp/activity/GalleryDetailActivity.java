package com.dci.clupapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.dci.clupapp.R;
import com.dci.clupapp.adapter.GalleryDetailAdapter;
import com.dci.clupapp.models.GalleryResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GalleryDetailActivity extends BaseActivity {
    @BindView(R.id.gallery_recycleview)
    RecyclerView gallery_recycleview;
    @BindView(R.id.back)
    ImageView back_arrow;
    private RecyclerView recyclerview;
    private List<GalleryResponse.GalleryResult> galList;
    private List<String> list;
    private GalleryDetailAdapter mAdapter;


    String image, name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        ButterKnife.bind(this);

        Intent i = getIntent();
        final ArrayList<String> stringArray = i.getStringArrayListExtra("data");

        mAdapter = new GalleryDetailAdapter(stringArray);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        gallery_recycleview.setLayoutManager(gridLayoutManager);
        gallery_recycleview.setItemAnimator(new DefaultItemAnimator());
        gallery_recycleview.setAdapter(mAdapter);

        mAdapter.setOnClickListen(new GalleryDetailAdapter.AddTouchListen() {
            @Override
            public void OnTouchClick(int position) {
                startActivity(new Intent(GalleryDetailActivity.this, ImageDetailActivity.class).putExtra("image", stringArray.get(position)));
            }
        });
    }

    @OnClick(R.id.back)
    public void onButtonClick(View view) {
        onBackPressed();
    }
/*
    private void AnalData() {
        GalleryDetailDTO s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.test));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.party));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.dance));
        galList.add(s);

        s = new GalleryDetailDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        galList.add(s);

    }
*/
}
