package com.dci.clupapp.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailActivity extends BaseActivity {

    private ImageView back, mImage;
    private TextView mTitle, mDesc, mDuration, mLocation;
    Button rsvp_btn;

    String name, description, date, location, image, status, eventId;

    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    CommonResponse commonResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        back = findViewById(R.id.back);
        mImage = findViewById(R.id.image);
        mTitle = findViewById(R.id.mTitle);
        mDesc = findViewById(R.id.mDesc);
        mDuration = findViewById(R.id.mDuration);
        mLocation = findViewById(R.id.mLocation);
        rsvp_btn = findViewById(R.id.rsvp_btn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        Intent i = getIntent();
        Bundle bundle = i.getBundleExtra("type");
        name = bundle.getString("name");
        description = bundle.getString("description");
        date = bundle.getString("date");
        location = bundle.getString("location");
        image = bundle.getString("image");
        status = bundle.getString("status");
        eventId = bundle.getString("eventId");

        mTitle.setText(name);
        mDesc.setText(description);
        mDuration.setText(getDateTimeFormat(date));
        mLocation.setText(location);
        Picasso.get().load(Constants.ImageUrl + image).placeholder(getResources().getDrawable(R.drawable.ic_place_holder)).fit().into(mImage);

        if (status.equalsIgnoreCase("1")) {
            rsvp_btn.setBackgroundColor(getResources().getColor(R.color.gray_bg));
            rsvp_btn.setVisibility(View.VISIBLE);
            rsvp_btn.setEnabled(false);
            rsvp_btn.setClickable(false);

        } else {
            rsvp_btn.setBackground(getResources().getDrawable(R.drawable.button_background));
            rsvp_btn.setVisibility(View.VISIBLE);
            rsvp_btn.setEnabled(true);
            rsvp_btn.setClickable(true);

        }

        rsvp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (status.equalsIgnoreCase("1")) {
                    Toast.makeText(EventDetailActivity.this, "Already Accepted", Toast.LENGTH_SHORT).show();

                } else {
                    EventAccept(eventId);
                }
            }
        });
    }

    public void EventAccept(String eventid) {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String appversion = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));

        clubAPI.acceptList(userid, eventid, deviceid, fcm_key, deviceos, appversion)
                .enqueue(new Callback<CommonResponse>() {


                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        try {
                            //  hideProgress();
                            if (response.body() != null) {
                                Log.e("response", ">>" + response.body());
                                if (commonResponse.getStatus().equals("Success")) {
                                    Toast.makeText(EventDetailActivity.this, "Updated Successfully", Toast.LENGTH_SHORT).show();
                                    rsvp_btn.setBackgroundColor(getResources().getColor(R.color.gray_bg));
                                } else if (commonResponse.getStatus().equals("fail")) {
                                    startActivity(new Intent(EventDetailActivity.this, MainActivity.class));
                                    Toast.makeText(EventDetailActivity.this, "Server Error", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Toast.makeText(EventDetailActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            startActivity(new Intent(EventDetailActivity.this, MainActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(EventDetailActivity.this, getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }

}
