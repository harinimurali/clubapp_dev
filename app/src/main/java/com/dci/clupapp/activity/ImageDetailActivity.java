package com.dci.clupapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.dci.clupapp.R;
import com.dci.clupapp.utils.Constants;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

public class ImageDetailActivity extends BaseActivity {

    private ZoomageView imageDetail;
    private String image;
    private int zoomControler=20;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);

        imageDetail=findViewById(R.id.imageDetail);
        back=findViewById(R.id.back);

        try {

            Intent i = getIntent();
            image = i.getStringExtra("image");
            Picasso.get().load(image).into(imageDetail);
            //  imageDetail.setFocusable(true);
        }catch (Exception e){
            e.printStackTrace();
        }

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
