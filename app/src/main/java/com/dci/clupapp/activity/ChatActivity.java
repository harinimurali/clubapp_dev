package com.dci.clupapp.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dci.clupapp.R;
import com.dci.clupapp.models.ChatMessageParams;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.dci.clupapp.utils.FontTextViewBold;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.firebase.ui.database.SnapshotParser;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.appindexing.Action;
import com.google.firebase.appindexing.FirebaseUserActions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends BaseActivity {


    @BindView(R.id.relative_chat)
    RelativeLayout relativeChat;
    @BindView(R.id.img_selectimage)
    ImageView imgSelectimage;
    @BindView(R.id.edit_msg)
    EditText editMsg;
    @BindView(R.id.img_send)
    ImageView imgSend;
    @BindView(R.id.relative_send)
    RelativeLayout relativeSend;
    @BindView(R.id.recycle_userlist)
    RecyclerView recycleUserlist;
    @BindView(R.id.text_chatname)
    FontTextViewBold textChatname;
    @BindView(R.id.img_back)
    ImageView imgBack;
    private Intent chatTypeIntent;
    private String chatWith;
    private int chatWithUserID;
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;
    public String RECENT_LIST_CHILD_SENDER, RECENT_LIST_CHILD_RECEIVER;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String ONE_TO_MANY_CHAT_MES_CHILD = "ONE_TO_MANY_CHAT_MESSAGES";
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private static final String LOADING_IMAGE_URL = "https://www.google.com/images/spin-32.gif";
    private String senderUserName;
    private String senderUniqueID, senderRoleName;
    private int senderUserID;
    private static final String MESSAGE_URL = "https://club-app-2e907.firebaseio.com/";
    private DatabaseReference messagesRef;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder> mFirebaseAdapter;
    private String chatMessage;
    private LinearLayoutManager mLinearLayoutManager;
    private boolean isForGroundExist, isTransparentModeOn;
    private static final int REQUEST_IMAGE = 1;
    private String imageUrl,sendePic,ReceiverPic,friendFcm;
    private String downloadChatImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mLinearLayoutManager.setStackFromEnd(true);
        senderUserName = AppPreferences.getProfile(ChatActivity.this).get(0).getResults().getFirstname();
        senderUserID = AppPreferences.getProfile(ChatActivity.this).get(0).getResults().getId();
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        chatTypeIntent = getIntent();
        if (getIntent() != null) {
            chatWith = chatTypeIntent.getStringExtra("chatWith");
            textChatname.setText(chatWith);
            chatWithUserID = chatTypeIntent.getIntExtra("chatWithUserID", 0);
            ReceiverPic = chatTypeIntent.getStringExtra("profileImage");
            friendFcm = chatTypeIntent.getStringExtra("fcmkey");
            sendePic = Constants.ImageUrl+AppPreferences.getProfile(ChatActivity.this).get(0).getResults().getProfileImage();

            //My node
            MESSAGES_CHILD_SENDER = senderUserID + "to" + chatWithUserID;
            //Receiver node
            MESSAGES_CHILD_RECEIVER = chatWithUserID + "to" + senderUserID;
            //Inbox node
            RECENT_LIST_CHILD_SENDER = "" + senderUserID;
            //Sent node
            RECENT_LIST_CHILD_RECEIVER = "" + chatWithUserID;
            messagesRef = mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).
                    child(MESSAGES_CHILD_SENDER);
            mFirebaseDatabaseReference.child("IS_FORE_GROUND").push().setValue(RECENT_LIST_CHILD_SENDER);

        }
        SnapshotParser<ChatMessageParams> parser = new SnapshotParser<ChatMessageParams>() {
            @Override
            public ChatMessageParams parseSnapshot(DataSnapshot dataSnapshot) {
                ChatMessageParams chatMessageParams = dataSnapshot.getValue(ChatMessageParams.class);
                if (chatMessageParams != null) {
                    chatMessageParams.setId(dataSnapshot.getKey());
                }
                return chatMessageParams;
            }
        };

        FirebaseRecyclerOptions<ChatMessageParams> options =
                new FirebaseRecyclerOptions.Builder<ChatMessageParams>()
                        .setQuery(messagesRef, parser)
                        .build();
        mFirebaseAdapter = new FirebaseRecyclerAdapter<ChatMessageParams, MessageViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull MessageViewHolder viewHolder, final int position, @NonNull ChatMessageParams chatMessageParams) {

                if (chatMessageParams.getText() != null) {
                    chatMessage = chatMessageParams.getText();
                } else if (chatMessageParams.getImageUrl() != null) {
                    imageUrl = chatMessageParams.getImageUrl();
                    if (imageUrl.startsWith("gs://")) {
                        StorageReference storageReference = FirebaseStorage.getInstance()
                                .getReferenceFromUrl(imageUrl);
                        storageReference.getDownloadUrl().addOnCompleteListener(
                                new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()) {
                                            downloadChatImageUrl = task.getResult().toString();
                                        } else {
                                            Log.w("chat", "Getting download url was not successful.",
                                                    task.getException());
                                        }
                                    }
                                });
                    } else {
                        downloadChatImageUrl = imageUrl;
                    }
                }

                if (chatMessageParams.getName().equalsIgnoreCase(senderUserName)) {
                    viewHolder.relative_left.setVisibility(View.GONE);
                    viewHolder.relative_right.setVisibility(View.VISIBLE);
                    viewHolder.textSentMesssgeRight.setText(chatMessageParams.getSentTime());
                    Picasso.get().load(chatMessageParams.getPhotoUrl()).placeholder(R.drawable.ic_user).into(viewHolder.senderImage);
                    if (chatMessageParams.getText() != null) {
                        viewHolder.messageImageViewRight.setVisibility(View.GONE);
                        viewHolder.textMesssgeRight.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeRight.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewRight.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeRight.setVisibility(View.GONE);
                        Glide.with(viewHolder.messageImageViewRight.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewRight);
                    }
                } else {
                    viewHolder.relative_left.setVisibility(View.VISIBLE);
                    viewHolder.relative_right.setVisibility(View.GONE);
                    viewHolder.textSentMesssgeLeft.setText(chatMessageParams.getSentTime());
                    Picasso.get().load(chatMessageParams.getReceiverPhotoUrl()).placeholder(R.drawable.ic_user).into(viewHolder.receiverImage);
                    if (chatMessageParams.getText() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.GONE);
                        viewHolder.textMesssgeLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setText(chatMessage);
                    } else if (chatMessageParams.getImageUrl() != null) {
                        viewHolder.messageImageViewLeft.setVisibility(View.VISIBLE);
                        viewHolder.textMesssgeLeft.setVisibility(View.GONE);
                        Glide.with(viewHolder.messageImageViewLeft.getContext())
                                .load(downloadChatImageUrl)
                                .into(viewHolder.messageImageViewLeft);
                    }

                }
                // log a view action on it
                FirebaseUserActions.getInstance().end(getMessageViewAction(chatMessageParams));
                viewHolder.messageImageViewLeft.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });
                viewHolder.messageImageViewRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ChatActivity.this);
                        LayoutInflater inflater = getLayoutInflater();
                        View dialogView = inflater.inflate(R.layout.alert_detail_image_view, null);
                        dialogBuilder.setView(dialogView);
                        ImageView imageView = (ImageView) dialogView.findViewById(R.id.image_detail_view);
                        AlertDialog alertDialog = dialogBuilder.create();
                        Glide.with(imageView.getContext())
                                .load(mFirebaseAdapter.getItem(position).getImageUrl())
                                .into(imageView);
                        alertDialog.show();
                    }
                });


            }

            @NonNull
            @Override
            public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
                //  showProgress();
                return new MessageViewHolder(inflater.inflate(R.layout.childlayout_chatmessage,
                        viewGroup, false));
            }
        };
        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    recycleUserlist.scrollToPosition(positionStart);
                }
            }
        });
        recycleUserlist.setLayoutManager(mLinearLayoutManager);
        recycleUserlist.setAdapter(mFirebaseAdapter);

    }

    @OnClick({R.id.img_selectimage, R.id.img_send, R.id.img_back})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_selectimage:
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_IMAGE);
                break;
            case R.id.img_send:
                sendMessage();
                break;
            case R.id.img_back:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("chat", "onActivityResult: requestCode=" + requestCode + ", resultCode=" + resultCode);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK) {
            getImageResult(data);
        }
    }

    private void getImageResult(Intent data) {
        if (data != null) {
            final Uri uri = data.getData();
            Log.d("chat", "Uri: " + uri.toString());
            //One to one
            ChatMessageParams tempMessage = new ChatMessageParams(senderUserID, "image", senderUserName,sendePic
                    , LOADING_IMAGE_URL, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", senderRoleName, "", "", "", friendFcm,
                    ReceiverPic, 0);
            ChatMessageParams tempMessage2 = new
                    ChatMessageParams(chatWithUserID, "image", chatWith,
                    ReceiverPic,
                    LOADING_IMAGE_URL, getCurrentTimeandDate(), senderUserName, senderUserID,
                    "", "", "",
                    "", "", friendFcm, sendePic, 0);


            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push()
                    .setValue(tempMessage, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError,
                                               DatabaseReference databaseReference) {
                            if (databaseError == null) {
                                String key = databaseReference.getKey();
                                StorageReference storageReference =
                                        FirebaseStorage.getInstance()
                                                .getReference()
                                                .child(key)
                                                .child(uri.getLastPathSegment());

                                putImageInStorage1(storageReference, uri, key);
                            } else {
                                Log.w("chat", "Unable to write message to database.",
                                        databaseError.toException());
                            }
                        }
                    });
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push()
                    .setValue(tempMessage2, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError,
                                               DatabaseReference databaseReference) {
                            if (databaseError == null) {
                                String key = databaseReference.getKey();
                                StorageReference storageReference =
                                        FirebaseStorage.getInstance()
                                                .getReference()
                                                .child(key)
                                                .child(uri.getLastPathSegment());

                                putImageInStorage2(storageReference, uri, key);
                            } else {
                                Log.w("chat", "Unable to write message to database.",
                                        databaseError.toException());
                            }
                        }
                    });
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_SENDER).
                    child(RECENT_LIST_CHILD_RECEIVER).
                    setValue(tempMessage);
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_RECEIVER).
                    child(RECENT_LIST_CHILD_SENDER).
                    setValue(tempMessage2);
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Notifications").push()
                    .setValue(tempMessage);

        }
    }

    private void putImageInStorage1(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, sendePic,
                                    downloadUri.toString(),
                                    getCurrentTimeandDate(), chatWith, chatWithUserID, "",
                                    senderRoleName, "", "", "", friendFcm,
                                    ReceiverPic, 0);
                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).child(key)
                            .setValue(chatMessageParams);
//                    //Sent items
//                    ChatMessageParams chatMessageParams2 = new
//                            ChatMessageParams(0, "", "",
//                            "", null, getCurrentTimeandDate(), chatWith, chatWithUserID, "",
//                            "", ""
//                            , memberID, designation, memberFCMToken, chatwithProPic);
//                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Sent").
//                            child(RECENT_LIST_CHILD_SENDER).push().
//                            setValue(chatMessageParams2);

                } else {
                    Log.w("chat", "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }

    private void putImageInStorage2(final StorageReference storageReference, Uri uri, final String key) {
        storageReference.putFile(uri).
                continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return storageReference.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    ChatMessageParams chatMessageParams =
                            new ChatMessageParams(senderUserID, null, senderUserName, sendePic,
                                    downloadUri.toString(), getCurrentTimeandDate(), chatWith, chatWithUserID,
                                    "", senderRoleName, ""
                                    , "", "", friendFcm, ReceiverPic, 0);

                    mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).child(key)
                            .setValue(chatMessageParams);
                    //Inbox
                    final ChatMessageParams chatMessageParams3 = new
                            ChatMessageParams(senderUserID, null, senderUserName,
                            sendePic,
                            null, getCurrentTimeandDate(), "", 0,
                            "", senderRoleName, "",
                            "", "", friendFcm, ReceiverPic, 0);

                    mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot snapshot) {
                                    if (snapshot.hasChild(RECENT_LIST_CHILD_RECEIVER)) {
                                        //Old User
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER)
                                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot snapshot) {
                                                        //Old User
                                                        for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {

                                                                //Check chat with user is foreground or background
                                                                mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                                        addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                                for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                                    if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                                        isForGroundExist = true;
                                                                                    } else {
                                                                                        isForGroundExist = false;
                                                                                    }
                                                                                    break;
                                                                                }
                                                                                if (!isForGroundExist) {
                                                                                    dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                                break;

                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(DatabaseError databaseError) {

                                                    }
                                                });
                                    } else {
//                                        New
                                        mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                                child(RECENT_LIST_CHILD_RECEIVER).push().
                                                setValue(chatMessageParams3);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                } else {
                    Log.w("chat", "Image upload task was not successful.",
                            task.getException());
                }
            }
        });
    }


    private Action getMessageViewAction(ChatMessageParams chatMessageParams) {
        return new Action.Builder(Action.Builder.VIEW_ACTION)
                .setObject(chatMessageParams.getName(), MESSAGE_URL.concat(chatMessageParams.getId()))
                .setMetadata(new Action.Metadata.Builder().setUpload(false))
                .build();
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView textMesssgeLeft, textMesssgeRight;
        TextView textSentMesssgeLeft, textSentMesssgeRight;
        ImageView messageImageViewLeft, messageImageViewRight;
        CircleImageView senderImage,receiverImage;
        RelativeLayout relative_left, relative_right;

        public MessageViewHolder(View v) {
            super(v);
            textMesssgeLeft = (TextView) itemView.findViewById(R.id.text_chat_mes_left);
            textMesssgeRight = (TextView) itemView.findViewById(R.id.text_chat_mes_right);
            textSentMesssgeRight = (TextView) itemView.findViewById(R.id.text_sent_time_right);
            textSentMesssgeLeft = (TextView) itemView.findViewById(R.id.text_sent_time_left);
            messageImageViewLeft = (ImageView) itemView.findViewById(R.id.image_chat_left);
            messageImageViewRight = (ImageView) itemView.findViewById(R.id.image_chat_right);
            senderImage =  itemView.findViewById(R.id.sender_image);
            receiverImage =  itemView.findViewById(R.id.receiver_image);
            relative_left = itemView.findViewById(R.id.relative_left);
            relative_right = itemView.findViewById(R.id.relative_right);
        }
    }

    private void sendMessage() {

        if (editMsg.getText() != null && editMsg.getText().toString().length() > 0) {
            //One to One
            //Chat list
            ChatMessageParams chatMessageParams = new
                    ChatMessageParams(senderUserID, editMsg.getText().toString(), senderUserName,
                    sendePic, null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", "", ""
                    , "", "", "", ReceiverPic, 1);
            //My node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_SENDER).push().
                    setValue(chatMessageParams);
            //Receiver node
            mFirebaseDatabaseReference.child(ONE_TO_ONE_CHAT_MES_CHILD).child(MESSAGES_CHILD_RECEIVER).push().
                    setValue(chatMessageParams);

            //notification
            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Notifications").push()
                    .setValue(chatMessageParams);

            //Inbox list
            final ChatMessageParams chatMessageParams3 = new
                    ChatMessageParams(senderUserID, editMsg.getText().toString().trim(), senderUserName,
                    sendePic,
                    null, getCurrentTimeandDate(), chatWith, chatWithUserID,
                    "", "", "",
                    "", "", "", ReceiverPic, 1);
            final ChatMessageParams chatMessageParams4 = new
                    ChatMessageParams(chatWithUserID, editMsg.getText().toString().trim(), chatWith,
                    ReceiverPic,
                    null, getCurrentTimeandDate(), senderUserName, senderUserID,
                    "", "", "",
                    "", "", "", sendePic, 1);


            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox")
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot snapshot) {

                            //Old User
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").
                                    child(RECENT_LIST_CHILD_RECEIVER)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(final DataSnapshot snapshot) {
                                            //Old User
                                            for (final DataSnapshot dataSnapshot : snapshot.getChildren()) {
                                                if (chatMessageParams3.getUniqueID().
                                                        equals(dataSnapshot.child("uniqueID").
                                                                getValue())) {
                                                    //Check chat with user is foreground or background
                                                    mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                                                            addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot1) {
                                                                    for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                                                                        if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_RECEIVER)) {
                                                                            isForGroundExist = true;
                                                                        } else {
                                                                            isForGroundExist = false;
                                                                        }
                                                                        break;
                                                                    }
                                                                    if (!isForGroundExist) {
                                                                        dataSnapshot.getRef().child("readStatus").setValue(0);
                                                                    }
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });
                                                    break;

                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });

                            //                                        New
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_SENDER).
                                    child(RECENT_LIST_CHILD_RECEIVER).
                                    setValue(chatMessageParams3);
                            mFirebaseDatabaseReference.child(RECENT_CHAT).child("Inbox").child(RECENT_LIST_CHILD_RECEIVER).
                                    child(RECENT_LIST_CHILD_SENDER).
                                    setValue(chatMessageParams4);


                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });


        } else {
            editMsg.setError("empty text");
        }
        editMsg.setText("");
    }

    public String getCurrentTimeandDate() {
        Date currentDate = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
        String currentTimeandTime = simpleDateFormat.format(currentDate);
        return currentTimeandTime;
    }

    @Override
    public void onResume() {
        super.onResume();
        mFirebaseAdapter.startListening();
    }

    @Override
    protected void onPause() {
        mFirebaseAdapter.stopListening();
        mFirebaseDatabaseReference.child("IS_FORE_GROUND").
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot1) {
                        for (final DataSnapshot dataSnapshot2 : dataSnapshot1.getChildren()) {
                            if (dataSnapshot2.getValue().equals(RECENT_LIST_CHILD_SENDER)) {
                                dataSnapshot2.getRef().removeValue();
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        super.onPause();
    }
}
