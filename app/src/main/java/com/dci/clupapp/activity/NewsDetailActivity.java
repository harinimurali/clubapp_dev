package com.dci.clupapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class NewsDetailActivity extends Activity {
    String id,title,description,post,image,date;
    private ImageView mImage,mShare,back;
    private TextView mTitle,mDesc,mPost,mDate;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ClubApplication.getContext().getComponent().inject(this);
        mImage=(ImageView)findViewById(R.id.mImage);
        mTitle=(TextView)findViewById(R.id.mTitle);
        mDesc=(TextView)findViewById(R.id.mDesc);
        mShare=(ImageView)findViewById(R.id.mShare);
        mPost=(TextView)findViewById(R.id.mPost);
        mDate=(TextView)findViewById(R.id.mDate);
        back=(ImageView)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hey check out my app at: https://play.google.com/store/apps/details?id=com.google.android.apps.plus");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
        editor = sharedPreferences.edit();
        Intent i=getIntent();
        Bundle bundle = i.getBundleExtra("data");
        id = bundle.getString("id");
        title = bundle.getString("title");
        description = bundle.getString("description");
        post = bundle.getString("post");
        image = bundle.getString("image");
        date = bundle.getString("date");

        mTitle.setText(title);
        mDesc.setText(description);
        mPost.setText(post);
        mDate.setText(date);
        Picasso.get().load(Constants.ImageUrl + image).fit().placeholder(getResources().getDrawable(R.drawable.ic_place_holder)).into(mImage);


    }
}
