package com.dci.clupapp.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.adapter.ContactListAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ChatMessageModel;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;
import com.dci.clupapp.utils.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendListActivity extends BaseActivity implements OnItemClickListener {

    @BindView(R.id.member_search)
    SearchView memberSearch;
    @BindView(R.id.relative_search)
    RelativeLayout relativeSearch;
    @BindView(R.id.recycle_userlist)
    RecyclerView recycleUserlist;

    private List<ChatMessageModel.Result> friendlist = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private ContactListAdapter contactListAdapter;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    private SharedPreferences fcmSharedPrefrences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_list);

        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = this.getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        ButterKnife.bind(this);

        memberSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getsearch(newText);
                return false;
            }
        });

        getresult();
    }

    private void getresult() {
        showProgress();
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.USERID, "");
        String vendorid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.friendlist(userid,vendorid,fcm_key,deviceid,osversion,appversion,"moto","48956156156121").
                enqueue(new Callback<ChatMessageModel>() {
                    @Override
                    public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                        recycleUserlist.setVisibility(View.VISIBLE);
                        hideProgress();
                        if (response.code()==200 && response.isSuccessful() && response.body() != null){
                            if (response.body().getStatus().equals("Success")){
                                friendlist = response.body().getResults();
                                if (friendlist.size()>0){
                                    mLayoutManager = new LinearLayoutManager(FriendListActivity.this,
                                            LinearLayoutManager.VERTICAL, false);
                                    recycleUserlist.setLayoutManager(mLayoutManager);
                                    recycleUserlist.setItemAnimator(new DefaultItemAnimator());
                                    contactListAdapter = new ContactListAdapter(friendlist, FriendListActivity.this);
                                    contactListAdapter.setItemclickListener(FriendListActivity.this);
                                    recycleUserlist.setAdapter(contactListAdapter);

                                }
                                else {
                                    recycleUserlist.setVisibility(View.GONE);
                                    Toast.makeText(FriendListActivity.this, "empty list", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {
                                recycleUserlist.setVisibility(View.GONE);
                                Toast.makeText(FriendListActivity.this, "failed", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {
                            recycleUserlist.setVisibility(View.GONE);
                            Toast.makeText(FriendListActivity.this, "api error", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ChatMessageModel> call, Throwable t) {
                        hideProgress();
                        recycleUserlist.setVisibility(View.GONE);
                        Toast.makeText(FriendListActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void getsearch(String text) {
        showProgress();
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.USERID, "");
        String vendorid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.searchlist(userid,vendorid,fcm_key,deviceid,osversion,appversion,"moto","48956156156121",text).
                enqueue(new Callback<ChatMessageModel>() {
                    @Override
                    public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                        recycleUserlist.setVisibility(View.VISIBLE);
                        hideProgress();
                        if (response.code()==200 && response.isSuccessful() && response.body() != null){
                            if (response.body().getStatus().equals("Success")){
                                friendlist = response.body().getResults();
                                if (friendlist.size()>0){
                                    mLayoutManager = new LinearLayoutManager(FriendListActivity.this,
                                            LinearLayoutManager.VERTICAL, false);
                                    recycleUserlist.setLayoutManager(mLayoutManager);
                                    recycleUserlist.setItemAnimator(new DefaultItemAnimator());
                                    contactListAdapter = new ContactListAdapter(friendlist, FriendListActivity.this);
                                    contactListAdapter.setItemclickListener(FriendListActivity.this);
                                    recycleUserlist.setAdapter(contactListAdapter);

                                }
                                else {
                                    recycleUserlist.setVisibility(View.GONE);
                                    Toast.makeText(FriendListActivity.this, "empty list", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {
                                recycleUserlist.setVisibility(View.GONE);
                                Toast.makeText(FriendListActivity.this, "failed", Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {
                            recycleUserlist.setVisibility(View.GONE);
                            Toast.makeText(FriendListActivity.this, "api error", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ChatMessageModel> call, Throwable t) {
                        hideProgress();
                        recycleUserlist.setVisibility(View.GONE);
                        Toast.makeText(FriendListActivity.this, "server error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void OnItemClick(int pos, View view) {
        ChatMessageModel.Result model = friendlist.get(pos);
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("chatWith", model.getFirstname());
        intent.putExtra("chatWithUserID", model.getId());
        intent.putExtra("profileImage", model.getProfile_image());
        intent.putExtra("fcmkey", model.getFcm());
        startActivity(intent);
        finish();

    }

    @Override
    public void OnItemClickSecond(int position, View view) {

    }
}
