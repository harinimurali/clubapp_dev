package com.dci.clupapp.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;


import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.dagger.AppModule;
import com.dci.clupapp.dagger.ApplicationComponent;
import com.dci.clupapp.dagger.DaggerApplicationComponent;
import com.dci.clupapp.retrofit.RetrofitModule;
import com.dci.clupapp.utils.Constants;

import javax.inject.Inject;

public class ClubApplication extends MultiDexApplication
{

    @Inject
    public SharedPreferences sharedPreferences;
    private static ClubApplication mInstance;
    public static ClubApplication getContext() {
        return mInstance;
    }
    private ApplicationComponent mComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance=this;
        mComponent = DaggerApplicationComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(Constants.CLUB_BASE_URL,getContext()))
                .build();
        mComponent.inject(this);

    }

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    public ApplicationComponent getComponent() {
        return mComponent;
    }

    public static ClubApplication from(@NonNull Context context) {
        return (ClubApplication) context.getApplicationContext();
    }
}
