package com.dci.clupapp.models;

import java.util.List;

public class ConnectResponse{
	private String Status;
	private List<ResultsItem> Results;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	private String error;

	public void setStatus(String status){
		this.Status = status;
	}

	public String getStatus(){
		return Status;
	}

	public void setResults(List<ResultsItem> results){
		this.Results = results;
	}

	public List<ResultsItem> getResults(){
		return Results;
	}

	public class ResultsItem{
		private String firstname;
		private String gender;
		private Object appVersion;
		private double distance;
		private String latitude;
		private String createdAt;
		private String weddingAnniversary;
		private Object fcm;
		private String profileImage;
		private Object deviceName;
		private String PositionHeld;
		private String updatedAt;
		private int classificationId;
		private int id;
		private int stateId;
		private String email;
		private String longitude;
		private Object deviceId;
		private String address2;
		private String address1;
		private Object appOs;
		private String mobile;
		private String spousename;
		private Object otp;
		private String classification;
		private String lastname;
		private String maritalStatus;
		private Object phone;
		private String dob;
		private int vendorId;
		private String onlinestatus;
		private Object imei;
		private Object otpsenttime;
		private int countryId;
		private int positionId;
		private int cityId;
		private int status;

		public void setFirstname(String firstname){
			this.firstname = firstname;
		}

		public String getFirstname(){
			return firstname;
		}

		public void setGender(String gender){
			this.gender = gender;
		}

		public String getGender(){
			return gender;
		}

		public void setAppVersion(Object appVersion){
			this.appVersion = appVersion;
		}

		public Object getAppVersion(){
			return appVersion;
		}

		public void setDistance(double distance){
			this.distance = distance;
		}

		public double getDistance(){
			return distance;
		}

		public void setLatitude(String latitude){
			this.latitude = latitude;
		}

		public String getLatitude(){
			return latitude;
		}

		public void setCreatedAt(String createdAt){
			this.createdAt = createdAt;
		}

		public String getCreatedAt(){
			return createdAt;
		}

		public void setWeddingAnniversary(String weddingAnniversary){
			this.weddingAnniversary = weddingAnniversary;
		}

		public String getWeddingAnniversary(){
			return weddingAnniversary;
		}

		public void setFcm(Object fcm){
			this.fcm = fcm;
		}

		public Object getFcm(){
			return fcm;
		}

		public void setProfileImage(String profileImage){
			this.profileImage = profileImage;
		}

		public String getProfileImage(){
			return profileImage;
		}

		public void setDeviceName(Object deviceName){
			this.deviceName = deviceName;
		}

		public Object getDeviceName(){
			return deviceName;
		}

		public void setPositionHeld(String positionHeld){
			this.PositionHeld = positionHeld;
		}

		public String getPositionHeld(){
			return PositionHeld;
		}

		public void setUpdatedAt(String updatedAt){
			this.updatedAt = updatedAt;
		}

		public String getUpdatedAt(){
			return updatedAt;
		}

		public void setClassificationId(int classificationId){
			this.classificationId = classificationId;
		}

		public int getClassificationId(){
			return classificationId;
		}

		public void setId(int id){
			this.id = id;
		}

		public int getId(){
			return id;
		}

		public void setStateId(int stateId){
			this.stateId = stateId;
		}

		public int getStateId(){
			return stateId;
		}

		public void setEmail(String email){
			this.email = email;
		}

		public String getEmail(){
			return email;
		}

		public void setLongitude(String longitude){
			this.longitude = longitude;
		}

		public String getLongitude(){
			return longitude;
		}

		public void setDeviceId(Object deviceId){
			this.deviceId = deviceId;
		}

		public Object getDeviceId(){
			return deviceId;
		}

		public void setAddress2(String address2){
			this.address2 = address2;
		}

		public String getAddress2(){
			return address2;
		}

		public void setAddress1(String address1){
			this.address1 = address1;
		}

		public String getAddress1(){
			return address1;
		}

		public void setAppOs(Object appOs){
			this.appOs = appOs;
		}

		public Object getAppOs(){
			return appOs;
		}

		public void setMobile(String mobile){
			this.mobile = mobile;
		}

		public String getMobile(){
			return mobile;
		}

		public void setSpousename(String spousename){
			this.spousename = spousename;
		}

		public String getSpousename(){
			return spousename;
		}

		public void setOtp(Object otp){
			this.otp = otp;
		}

		public Object getOtp(){
			return otp;
		}

		public void setClassification(String classification){
			this.classification = classification;
		}

		public String getClassification(){
			return classification;
		}

		public void setLastname(String lastname){
			this.lastname = lastname;
		}

		public String getLastname(){
			return lastname;
		}

		public void setMaritalStatus(String maritalStatus){
			this.maritalStatus = maritalStatus;
		}

		public String getMaritalStatus(){
			return maritalStatus;
		}

		public void setPhone(Object phone){
			this.phone = phone;
		}

		public Object getPhone(){
			return phone;
		}

		public void setDob(String dob){
			this.dob = dob;
		}

		public String getDob(){
			return dob;
		}

		public void setVendorId(int vendorId){
			this.vendorId = vendorId;
		}

		public int getVendorId(){
			return vendorId;
		}

		public void setOnlinestatus(String onlinestatus){
			this.onlinestatus = onlinestatus;
		}

		public String getOnlinestatus(){
			return onlinestatus;
		}

		public void setImei(Object imei){
			this.imei = imei;
		}

		public Object getImei(){
			return imei;
		}

		public void setOtpsenttime(Object otpsenttime){
			this.otpsenttime = otpsenttime;
		}

		public Object getOtpsenttime(){
			return otpsenttime;
		}

		public void setCountryId(int countryId){
			this.countryId = countryId;
		}

		public int getCountryId(){
			return countryId;
		}

		public void setPositionId(int positionId){
			this.positionId = positionId;
		}

		public int getPositionId(){
			return positionId;
		}

		public void setCityId(int cityId){
			this.cityId = cityId;
		}

		public int getCityId(){
			return cityId;
		}

		public void setStatus(int status){
			this.status = status;
		}

		public int getStatus(){
			return status;
		}
	}

}