package com.dci.clupapp.models;

import java.util.List;

public class MusicResponse{
	private String Status;
	private MusicResults Results;

	public void setStatus(String status){
		this.Status = status;
	}

	public String getStatus(){
		return Status;
	}

	public void setResults(MusicResults results){
		this.Results = results;
	}

	public MusicResults getResults(){
		return Results;
	}

	public class MusicResults  {
		private String path;
		private String perPage;
		private int total;
		private List<MusicDataItem> data;
		private int lastPage;
		private Object nextPageUrl;
		private int from;
		private int to;
		private Object prevPageUrl;
		private int currentPage;

		public void setPath(String path){
			this.path = path;
		}

		public String getPath(){
			return path;
		}

		public void setPerPage(String perPage){
			this.perPage = perPage;
		}

		public String getPerPage(){
			return perPage;
		}

		public void setTotal(int total){
			this.total = total;
		}

		public int getTotal(){
			return total;
		}

		public void setData(List<MusicDataItem> data){
			this.data = data;
		}

		public List<MusicDataItem> getData(){
			return data;
		}

		public void setLastPage(int lastPage){
			this.lastPage = lastPage;
		}

		public int getLastPage(){
			return lastPage;
		}

		public void setNextPageUrl(Object nextPageUrl){
			this.nextPageUrl = nextPageUrl;
		}

		public Object getNextPageUrl(){
			return nextPageUrl;
		}

		public void setFrom(int from){
			this.from = from;
		}

		public int getFrom(){
			return from;
		}

		public void setTo(int to){
			this.to = to;
		}

		public int getTo(){
			return to;
		}

		public void setPrevPageUrl(Object prevPageUrl){
			this.prevPageUrl = prevPageUrl;
		}

		public Object getPrevPageUrl(){
			return prevPageUrl;
		}

		public void setCurrentPage(int currentPage){
			this.currentPage = currentPage;
		}

		public int getCurrentPage(){
			return currentPage;
		}

		public class MusicDataItem{
			private String updated_at;
			private int vendorId;
			private String createdAt;
			private int id;
			private String title;
			private String url;
			private int status;

			public void setUpdatedAt(String updatedAt){
				this.updated_at = updatedAt;
			}

			public String getUpdatedAt(){
				return updated_at;
			}

			public void setVendorId(int vendorId){
				this.vendorId = vendorId;
			}

			public int getVendorId(){
				return vendorId;
			}

			public void setCreatedAt(String createdAt){
				this.createdAt = createdAt;
			}

			public String getCreatedAt(){
				return createdAt;
			}

			public void setId(int id){
				this.id = id;
			}

			public int getId(){
				return id;
			}

			public void setTitle(String title){
				this.title = title;
			}

			public String getTitle(){
				return title;
			}

			public void setUrl(String url){
				this.url = url;
			}

			public String getUrl(){
				return url;
			}

			public void setStatus(int status){
				this.status = status;
			}

			public int getStatus(){
				return status;
			}
		}

	}
}
