package com.dci.clupapp.models;

import java.util.List;

public class AboutUsResponse {
    private String Status;
    private List<ResultsItem> Results;

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setResults(List<ResultsItem> results) {
        this.Results = results;
    }

    public List<ResultsItem> getResults() {
        return Results;
    }

    public class ResultsItem {
        private String address;
        private String updatedAt;
        private String contact;
        private String description;
        private String createdAt;
        private String mailid;
        private int id;
        private String title;
        private int status;

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return address;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setContact(String contact) {
            this.contact = contact;
        }

        public String getContact() {
            return contact;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setMailid(String mailid) {
            this.mailid = mailid;
        }

        public String getMailid() {
            return mailid;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }
    }

}