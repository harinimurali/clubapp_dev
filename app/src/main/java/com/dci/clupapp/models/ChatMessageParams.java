/**
 * Copyright Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.dci.clupapp.models;

public class ChatMessageParams {

    private String id;
    private String text;
    private String name;
    private String photoUrl;
    private String imageUrl;

    public ChatMessageParams(String name) {
        this.name = name;
    }

    public int getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(int readStatus) {
        this.readStatus = readStatus;
    }

    private int readStatus;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    private String roleName;
    private String fcmToken;

    public String getReceiverRoleName() {
        return receiverRoleName;
    }

    public void setReceiverRoleName(String receiverRoleName) {
        this.receiverRoleName = receiverRoleName;
    }

    public String getReceiverFcmToken() {
        return receiverFcmToken;
    }

    public void setReceiverFcmToken(String receiverFcmToken) {
        this.receiverFcmToken = receiverFcmToken;
    }

    private String receiverRoleName;
    private String receiverFcmToken;

    public String getReceiverPhotoUrl() {
        return receiverPhotoUrl;
    }

    public void setReceiverPhotoUrl(String receiverPhotoUrl) {
        this.receiverPhotoUrl = receiverPhotoUrl;
    }

    private String receiverPhotoUrl;


    public ChatMessageParams(int senderUserID, String text, String name, String photoUrl,
                             String imageUrl, String sentTime, String toName, int chatWithID,
                             String uniqueID, String roleName, String fcmToken,
                             String receiverUniqueID, String receiverRoleName, String receiverFcmToken,
                             String receiverPhotoUrl, int readStatus) {
        this.text = text;
        this.name = name;
        this.photoUrl = photoUrl;
        this.imageUrl = imageUrl;
        this.sentTime = sentTime;
        this.toName = toName;
        this.chatWithID = chatWithID;
        this.senderUserID = senderUserID;
        this.uniqueID = uniqueID;
        this.roleName = roleName;
        this.fcmToken = fcmToken;
        this.receiverUniqueID = receiverUniqueID;
        this.receiverRoleName = receiverRoleName;
        this.receiverFcmToken = receiverFcmToken;
        this.receiverPhotoUrl = receiverPhotoUrl;
        this.readStatus = readStatus;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    private String uniqueID;

    public String getReceiverUniqueID() {
        return receiverUniqueID;
    }

    public void setReceiverUniqueID(String receiverUniqueID) {
        this.receiverUniqueID = receiverUniqueID;
    }

    private String receiverUniqueID;

    public int getSenderUserID() {
        return senderUserID;
    }

    public void setSenderUserID(int senderUserID) {
        this.senderUserID = senderUserID;
    }

    private int senderUserID;

    public int getChatWithID() {
        return chatWithID;
    }

    public void setChatWithID(int chatWithID) {
        this.chatWithID = chatWithID;
    }

    private int chatWithID;

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    private String toName;

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }

    private String sentTime;

    public ChatMessageParams() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
