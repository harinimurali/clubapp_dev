package com.dci.clupapp.models;

import java.util.List;

public class ChatMessageModel {
    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public List<Result> getResults() {
        return Results;
    }

    public void setResults(List<Result> results) {
        Results = results;
    }

    private String Status;
    private List<Result> Results = null;



    public class Result {

        private Integer id;
        private String firstname;
        private String lastname;
        private String email;
        private String mobile;
        private String phone;
        private String dob;
        private String gender;
        private String marital_status;
        private String spousename;
        private Integer classification_id;
        private Integer position_id;
        private String profile_image;
        private String address1;
        private String address2;
        private Integer city_id;
        private Integer state_id;
        private Integer country_id;
        private String latitude;
        private String longitude;
        private String wedding_anniversary;
        private Integer vendor_id;
        private String created_at;
        private String updated_at;
        private String app_version;
        private String app_os;
        private String fcm;
        private String imei;
        private String otp;
        private String device_name;
        private String device_id;
        private String otpsenttime;
        private String onlinestatus;
        private Integer status;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getMarital_status() {
            return marital_status;
        }

        public void setMarital_status(String marital_status) {
            this.marital_status = marital_status;
        }

        public String getSpousename() {
            return spousename;
        }

        public void setSpousename(String spousename) {
            this.spousename = spousename;
        }

        public Integer getClassification_id() {
            return classification_id;
        }

        public void setClassification_id(Integer classification_id) {
            this.classification_id = classification_id;
        }

        public Integer getPosition_id() {
            return position_id;
        }

        public void setPosition_id(Integer position_id) {
            this.position_id = position_id;
        }

        public String getProfile_image() {
            return profile_image;
        }

        public void setProfile_image(String profile_image) {
            this.profile_image = profile_image;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public Integer getCity_id() {
            return city_id;
        }

        public void setCity_id(Integer city_id) {
            this.city_id = city_id;
        }

        public Integer getState_id() {
            return state_id;
        }

        public void setState_id(Integer state_id) {
            this.state_id = state_id;
        }

        public Integer getCountry_id() {
            return country_id;
        }

        public void setCountry_id(Integer country_id) {
            this.country_id = country_id;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getWedding_anniversary() {
            return wedding_anniversary;
        }

        public void setWedding_anniversary(String wedding_anniversary) {
            this.wedding_anniversary = wedding_anniversary;
        }

        public Integer getVendor_id() {
            return vendor_id;
        }

        public void setVendor_id(Integer vendor_id) {
            this.vendor_id = vendor_id;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getApp_version() {
            return app_version;
        }

        public void setApp_version(String app_version) {
            this.app_version = app_version;
        }

        public String getApp_os() {
            return app_os;
        }

        public void setApp_os(String app_os) {
            this.app_os = app_os;
        }

        public String getFcm() {
            return fcm;
        }

        public void setFcm(String fcm) {
            this.fcm = fcm;
        }

        public String getImei() {
            return imei;
        }

        public void setImei(String imei) {
            this.imei = imei;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

        public String getDevice_name() {
            return device_name;
        }

        public void setDevice_name(String device_name) {
            this.device_name = device_name;
        }

        public String getDevice_id() {
            return device_id;
        }

        public void setDevice_id(String device_id) {
            this.device_id = device_id;
        }

        public String getOtpsenttime() {
            return otpsenttime;
        }

        public void setOtpsenttime(String otpsenttime) {
            this.otpsenttime = otpsenttime;
        }

        public String getOnlinestatus() {
            return onlinestatus;
        }

        public void setOnlinestatus(String onlinestatus) {
            this.onlinestatus = onlinestatus;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

    }
}
