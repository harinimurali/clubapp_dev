package com.dci.clupapp.models;

import java.util.List;

public class ProjectResponse {
    private String Status;
    private ProjectRes Results;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String error;

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setResults(ProjectRes results) {
        this.Results = results;
    }

    public ProjectRes getResults() {
        return Results;
    }

    public class ProjectRes {
        private String firstPageUrl;
        private String path;
        private String perPage;
        private int total;
        private List<DataRes> data;
        private int lastPage;
        private String lastPageUrl;
        private String nextPageUrl;
        private int from;
        private int to;
        private Object prevPageUrl;
        private int currentPage;

        public void setFirstPageUrl(String firstPageUrl) {
            this.firstPageUrl = firstPageUrl;
        }

        public String getFirstPageUrl() {
            return firstPageUrl;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getPath() {
            return path;
        }

        public void setPerPage(String perPage) {
            this.perPage = perPage;
        }

        public String getPerPage() {
            return perPage;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getTotal() {
            return total;
        }

        public void setData(List<DataRes> data) {
            this.data = data;
        }

        public List<DataRes> getData() {
            return data;
        }

        public void setLastPage(int lastPage) {
            this.lastPage = lastPage;
        }

        public int getLastPage() {
            return lastPage;
        }

        public void setLastPageUrl(String lastPageUrl) {
            this.lastPageUrl = lastPageUrl;
        }

        public String getLastPageUrl() {
            return lastPageUrl;
        }

        public void setNextPageUrl(String nextPageUrl) {
            this.nextPageUrl = nextPageUrl;
        }

        public String getNextPageUrl() {
            return nextPageUrl;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getFrom() {
            return from;
        }

        public void setTo(int to) {
            this.to = to;
        }

        public int getTo() {
            return to;
        }

        public void setPrevPageUrl(Object prevPageUrl) {
            this.prevPageUrl = prevPageUrl;
        }

        public Object getPrevPageUrl() {
            return prevPageUrl;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getCurrentPage() {
            return currentPage;
        }
    }

    public static class DataRes {
        private String venue;
        private String address2;
        private String address1;
        private String latitude;
        private String description;
        private String createdAt;
        private String Image;
        private String endDate;
        private String StartDate;
        private String updated_at;
        private int vendorId;
        private String name;
        private int id;
        private int stateId;
        private int countryId;
        private int cityId;
        private String longitude;
        private int status;

        public void setVenue(String venue) {
            this.venue = venue;
        }

        public String getVenue() {
            return venue;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress1() {
            return address1;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setImage(String image) {
            this.Image = image;
        }

        public String getImage() {
            return Image;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setStartDate(String startDate) {
            this.StartDate = startDate;
        }

        public String getStartDate() {
            return StartDate;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updated_at = updatedAt;
        }

        public String getUpdatedAt() {
            return updated_at;
        }

        public void setVendorId(int vendorId) {
            this.vendorId = vendorId;
        }

        public int getVendorId() {
            return vendorId;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setStateId(int stateId) {
            this.stateId = stateId;
        }

        public int getStateId() {
            return stateId;
        }

        public void setCountryId(int countryId) {
            this.countryId = countryId;
        }

        public int getCountryId() {
            return countryId;
        }

        public void setCityId(int cityId) {
            this.cityId = cityId;
        }

        public int getCityId() {
            return cityId;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getStatus() {
            return status;
        }
    }


}
