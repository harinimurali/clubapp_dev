package com.dci.clupapp.models;

import java.util.List;

public class NewsResponse {
    private String Status;
    private Results Results;

    public void setStatus(String status) {
        this.Status = status;
    }

    public String getStatus() {
        return Status;
    }

    public void setResults(Results results) {
        this.Results = results;
    }

    public Results getResults() {
        return Results;
    }

    public class Results {
        private List<TrendingItem> trending;
        private List<MonthItem> month;
        private List<TodayItem> today;

        public void setTrending(List<TrendingItem> trending) {
            this.trending = trending;
        }

        public List<TrendingItem> getTrending() {
            return trending;
        }

        public void setMonth(List<MonthItem> month) {
            this.month = month;
        }

        public List<MonthItem> getMonth() {
            return month;
        }

        public void setToday(List<TodayItem> today) {
            this.today = today;
        }

        public List<TodayItem> getToday() {
            return today;
        }

        public class TodayItem {
            private String date;
            private String image;
            private String updated_at;
            private int vendorId;
            private String createdAt;
            private int id;
            private String title;
            private String content;
            private int status;

            public void setDate(String date) {
                this.date = date;
            }

            public String getDate() {
                return date;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage() {
                return image;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updated_at = updatedAt;
            }

            public String getUpdatedAt() {
                return updated_at;
            }

            public void setVendorId(int vendorId) {
                this.vendorId = vendorId;
            }

            public int getVendorId() {
                return vendorId;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getId() {
                return id;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getContent() {
                return content;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getStatus() {
                return status;
            }
        }

        public class TrendingItem {
            private String date;
            private String image;
            private String updated_at;
            private int vendorId;
            private String createdAt;
            private int id;
            private String title;
            private String content;
            private int status;

            public void setDate(String date) {
                this.date = date;
            }

            public String getDate() {
                return date;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage() {
                return image;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updated_at = updatedAt;
            }

            public String getUpdatedAt() {
                return updated_at;
            }

            public void setVendorId(int vendorId) {
                this.vendorId = vendorId;
            }

            public int getVendorId() {
                return vendorId;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getId() {
                return id;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getContent() {
                return content;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getStatus() {
                return status;
            }
        }

        public class MonthItem {
            private String date;
            private String image;
            private String updated_at;
            private int vendorId;
            private String createdAt;
            private int id;
            private String title;
            private String content;
            private int status;

            public void setDate(String date) {
                this.date = date;
            }

            public String getDate() {
                return date;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getImage() {
                return image;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updated_at = updatedAt;
            }

            public String getUpdatedAt() {
                return updated_at;
            }

            public void setVendorId(int vendorId) {
                this.vendorId = vendorId;
            }

            public int getVendorId() {
                return vendorId;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getId() {
                return id;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getTitle() {
                return title;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getContent() {
                return content;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public int getStatus() {
                return status;
            }
        }
    }

}
