package com.dci.clupapp.models;

/**
 * Created by keerthana on 11/16/2018.
 */

public class GalleryDTO {

    private String img,event,album_count;



    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }



    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


    public String getAlbum_count() {
        return album_count;
    }

    public void setAlbum_count(String album_count) {
        this.album_count = album_count;
    }


}
