package com.dci.clupapp.models;

import java.util.List;

public class GalleryResponse{
    private String Status;

    public List<GalleryResult> getResults() {
        return Results;
    }

    public void setResults(List<GalleryResult> results) {
        Results = results;
    }

    private List<GalleryResult> Results;

    public void setStatus(String status){
        this.Status = Status;
    }

    public String getStatus(){
        return Status;
    }



    public static class GalleryResult{
        private String updated_at;
        private List<GalleryAttachment> attachment;
        private int vendorId;
        private String name;
        private String description;
        private String createdAt;
        private int id;
        private int status;

        public void setUpdatedAt(String updatedAt){
            this.updated_at = updatedAt;
        }

        public String getUpdatedAt(){
            return updated_at;
        }

        public void setAttachment(List<GalleryAttachment> attachment){
            this.attachment = attachment;
        }

        public List<GalleryAttachment> getAttachment(){
            return attachment;
        }

        public void setVendorId(int vendorId){
            this.vendorId = vendorId;
        }

        public int getVendorId(){
            return vendorId;
        }

        public void setName(String name){
            this.name = name;
        }

        public String getName(){
            return name;
        }

        public void setDescription(String description){
            this.description = description;
        }

        public String getDescription(){
            return description;
        }

        public void setCreatedAt(String createdAt){
            this.createdAt = createdAt;
        }

        public String getCreatedAt(){
            return createdAt;
        }

        public void setId(int id){
            this.id = id;
        }

        public int getId(){
            return id;
        }

        public void setStatus(int status){
            this.status = status;
        }

        public int getStatus(){
            return status;
        }

        public class GalleryAttachment{
            private String file_path;
            private String updated_at;
            private String fileType;
            private String createdAt;
            private int galleryId;
            private int id;
            private int status;

            public void setFilePath(String filePath){
                this.file_path = filePath;
            }

            public String getFilePath(){
                return file_path;
            }

            public void setUpdatedAt(String updatedAt){
                this.updated_at = updatedAt;
            }

            public String getUpdatedAt(){
                return updated_at;
            }

            public void setFileType(String fileType){
                this.fileType = fileType;
            }

            public String getFileType(){
                return fileType;
            }

            public void setCreatedAt(String createdAt){
                this.createdAt = createdAt;
            }

            public String getCreatedAt(){
                return createdAt;
            }

            public void setGalleryId(int galleryId){
                this.galleryId = galleryId;
            }

            public int getGalleryId(){
                return galleryId;
            }

            public void setId(int id){
                this.id = id;
            }

            public int getId(){
                return id;
            }

            public void setStatus(int status){
                this.status = status;
            }

            public int getStatus(){
                return status;
            }
        }


    }











}