package com.dci.clupapp.dagger;

import android.content.Context;
import android.content.SharedPreferences;

import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.retrofit.ClubAPI;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

import static android.content.Context.MODE_PRIVATE;

@Module
public class AppModule {

    public static final String CLUB_PREFS = "club";

    private final ClubApplication clubApp;

    public AppModule(ClubApplication app) {
        this.clubApp = app;
    }
    //provides dependencies (return application class objects)
    @Provides
    @Singleton
    Context provideApplicationContext() {
        return clubApp;
    }

    //provides dependencies (return sharedprference objects)
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences() {
        return clubApp.getSharedPreferences(CLUB_PREFS, MODE_PRIVATE);
    }

    //provides dependencies (return API class objects)
    @Provides
    public ClubAPI provideViduApiInterface(Retrofit retrofit) {
        return retrofit.create(ClubAPI.class);
    }
}
