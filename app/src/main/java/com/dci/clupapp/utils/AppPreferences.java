package com.dci.clupapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dci.clupapp.models.ProfileResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by harini on 12/3/2018.
 */

public class AppPreferences {
    public static void setProfile(Context c, List<ProfileResponse> cards) {
        if (cards != null) {
            String json = new Gson().toJson(cards, new TypeToken<List<ProfileResponse>>() {
            }.getType());
            PreferenceManager.getDefaultSharedPreferences(c).
                    edit().putString(Constants.PROFILE, json).commit();
        }
    }

    public static List<ProfileResponse> getProfile(Context c) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
        String placesJson = sp.getString(Constants.PROFILE, "");
        List<ProfileResponse> cards;
        cards = new Gson().fromJson(placesJson, new TypeToken<List<ProfileResponse>>() {
        }.getType());
        return cards;
    }

    public static boolean isLoggedIn(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(Constants.LOGIN_STATUS, false);
    }

    public static void setLoginStatus(Context context, boolean value) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor ed = sp.edit();
        ed.putBoolean(Constants.LOGIN_STATUS, value);
        ed.commit();
    }

    public static boolean getLoginStatus(Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getBoolean(Constants.LOGIN_STATUS, false);

    }
}
