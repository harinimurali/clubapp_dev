package com.dci.clupapp.utils;

/**
 * Created by harini on 8/24/2018.
 */

public interface AddTouchListen {
    public void onTouchClick(int position);

}