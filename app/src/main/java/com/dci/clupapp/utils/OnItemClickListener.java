package com.dci.clupapp.utils;

import android.view.View;


public interface OnItemClickListener {
    public void OnItemClick(int pos, View view);

     public void OnItemClickSecond(int position, View view);
}
