package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.activity.NewsDetailActivity;
import com.dci.clupapp.adapter.NewsTrendingAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.NewsResponse;
import com.dci.clupapp.models.NewsTrendingDTO;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewsFragment extends BaseFragment {

    private NewsTrendingAdapter newsListAdapter;
    private List<NewsResponse.Results.TrendingItem> newsTrendList;
    private List<NewsResponse.Results.TodayItem> newsTodayList;
    private List<NewsResponse.Results.MonthItem> newsMonthlyList;
    private RecyclerView newsListRecycle;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    NewsResponse newsResponse;
    private TextView no_data;


    public NewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);


        newsListRecycle = view.findViewById(R.id.news_recyclerview);
        no_data=view.findViewById(R.id.no_data);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        newsAPI();


        viewPager = (ViewPager) view.findViewById(R.id.viewpager);


        tabLayout = (TabLayout) view.findViewById(R.id.tabs);



        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        //   GalleryFragment.PagerAdapter adapter = new PagerAdapter(getFragmentManager());
        adapter.addFragment(new NewsTodayFragment(newsTodayList), "Today's News");
        adapter.addFragment(new NewsMonthFragment(newsMonthlyList), "This Month");
        viewPager.setAdapter(adapter);


    }

    public class PagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public PagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

/*
    private void NewsData() {
        NewsTrendingDTO s = new NewsTrendingDTO();
        //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s = new NewsTrendingDTO();
        //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s = new NewsTrendingDTO();
        //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);

        s = new NewsTrendingDTO();
        //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("set ut perspecticks unde omnis iste nature error sit accustanium,set ut perspecticks unde omnis iste nature error sit accustanium");
        newsList.add(s);
    }
*/

    public void newsAPI() {
        showProgress();
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String appversion = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));

        clubAPI.newsList(userid, fcm_key, deviceid, deviceos, "android", deviceid, appversion)
                .enqueue(new Callback<NewsResponse>() {
                    @Override
                    public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {

                        try {
                            hideProgress();
                            if (response.body() != null) {
                                Log.e("response", ">>" + response.body());
                                newsResponse = response.body();
                                if (newsResponse.getStatus().equals("Success")) {
                                    newsTrendList = new ArrayList<>();
                                    newsTrendList = newsResponse.getResults().getTrending();
                                    newsTodayList = newsResponse.getResults().getToday();
                                    newsMonthlyList = newsResponse.getResults().getMonth();
                                    newsListAdapter = new NewsTrendingAdapter(newsTrendList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
                                    newsListRecycle.setLayoutManager(mLayoutManager);
                                    newsListRecycle.setItemAnimator(new DefaultItemAnimator());
                                    newsListRecycle.setAdapter(newsListAdapter);

                                    newsListAdapter.setOnClickListen(new AddTouchListen() {
                                        @Override
                                        public void onTouchClick(int position) {

                                            Bundle bundle = new Bundle();
                                            bundle.putString("id",String.valueOf(newsTrendList.get(position).getId()));
                                            bundle.putString("title", String.valueOf(newsTrendList.get(position).getTitle()));
                                            bundle.putString("description", String.valueOf(newsTrendList.get(position).getContent()));
                                            bundle.putString("post", AppPreferences.getProfile(getContext()).get(position).getResults().getFirstname());
                                            bundle.putString("image", String.valueOf(newsTrendList.get(position).getImage()));
                                            bundle.putString("date", getTimeFormat(String.valueOf(newsTrendList.get(position).getUpdatedAt())));
                                            Intent intent=new Intent(getActivity(), NewsDetailActivity.class);
                                            intent.putExtra("data",bundle);
                                            startActivity(intent);
                                        }
                                    });

                                    setupViewPager(viewPager);
                                    tabLayout.setupWithViewPager(viewPager);
                                } else if (newsResponse.getStatus().equals("Failed")) {
//                                    newsListRecycle.setVisibility(View.GONE);
//                                    no_data.setVisibility(View.VISIBLE);
                                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                                }


                            } else {
//                                newsListRecycle.setVisibility(View.GONE);
//                                no_data.setVisibility(View.VISIBLE);

                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onFailure(Call<NewsResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.news));
    }
}
