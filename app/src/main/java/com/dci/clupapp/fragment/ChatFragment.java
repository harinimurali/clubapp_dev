package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.ChatActivity;
import com.dci.clupapp.activity.FriendListActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.RecentChatListAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ChatMessageParams;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment {


    private String senderUserName;
    ListView list_user_list;
    RelativeLayout relative_info;
    private int senderUserID;
    public String RECENT_LIST_CHILD;
    ImageView image_no_network;
    TextView text_no_network;
    SearchView member_search;
    FloatingActionButton fab_friendlist;
    // CircleImageView fab_friendlist;
    private DatabaseReference mFirebaseDatabaseReferenceInbox;
    public String RECENT_CHAT = "RECENT_CHAT_MESSAGES";
    private DatabaseReference messagesRef;
    private ArrayList<ChatMessageParams> membersReceiverList;
    RecentChatListAdapter reecentChatListAdapter;
    public String ONE_TO_ONE_CHAT_MES_CHILD = "ONE_TO_ONE_CHAT_MESSAGES";
    public String MESSAGES_CHILD_SENDER, MESSAGES_CHILD_RECEIVER;

    @Inject
    ClubAPI clubAPI;
    @Inject
    public SharedPreferences fcmSharedPrefrences;
    public SharedPreferences.Editor editor;

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        ClubApplication.getContext().getComponent().inject(this);
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);

        list_user_list = view.findViewById(R.id.list_user_list);
        relative_info = view.findViewById(R.id.relative_info);
        image_no_network = view.findViewById(R.id.image_no_network);
        text_no_network = view.findViewById(R.id.text_no_network);
        member_search = view.findViewById(R.id.member_search);
        fab_friendlist = view.findViewById(R.id.fab_friendlist);
        membersReceiverList = new ArrayList<ChatMessageParams>();
        senderUserName = AppPreferences.getProfile(getActivity()).get(0).getResults().getFirstname();
        senderUserID = AppPreferences.getProfile(getActivity()).get(0).getResults().getId();
        ;
        RECENT_LIST_CHILD = "" + senderUserID;


        showProgress();

        mFirebaseDatabaseReferenceInbox = FirebaseDatabase.getInstance().getReference();
        messagesRef = mFirebaseDatabaseReferenceInbox.child(RECENT_CHAT).child("Inbox")
                .child(RECENT_LIST_CHILD);

        messagesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                hideProgress();
                Log.i("snapshot", snapshot.toString());
                if (snapshot.getChildrenCount() > 0) {
                    Log.i("snapshot", "" + snapshot.getChildrenCount());
                    list_user_list.setVisibility(View.VISIBLE);
                    relative_info.setVisibility(View.INVISIBLE);
                    membersReceiverList.clear();
                    try {
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Log.i("snapshot1", "" + postSnapshot.getValue());
                            ChatMessageParams chatMessageParams = postSnapshot.getValue(ChatMessageParams.class);
                            Log.i("chatmessage", "to :" + chatMessageParams.getToName() + "name :" + chatMessageParams.getName().toString());
                            membersReceiverList.add(chatMessageParams);
                            // here you can access to name property like university.name

                        }
                        reecentChatListAdapter = new RecentChatListAdapter(getActivity(), membersReceiverList);
                        list_user_list.setAdapter(reecentChatListAdapter);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    list_user_list.setVisibility(View.GONE);
                    relative_info.setVisibility(View.VISIBLE);
                    text_no_network.setText("No chats, Tap the plus icon to chat");
                    image_no_network.setImageResource(R.drawable.icon_no_network);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getMessage());
            }
        });

        fab_friendlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FriendListActivity.class);
                startActivity(intent);
            }
        });

        list_user_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int postion, long l) {
                Intent intent = new Intent(getActivity(), ChatActivity.class);

                intent.putExtra("chatWith", membersReceiverList.get(postion).getToName());
                intent.putExtra("chatWithUserID", membersReceiverList.get(postion).getChatWithID());

                startActivity(intent);

                messagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        //Old User
                        for (DataSnapshot dataSnapshot : snapshot.getChildren()) {
                            if (membersReceiverList.get(postion).getUniqueID().
                                    equals(dataSnapshot.child("uniqueID").
                                            getValue())) {
                                dataSnapshot.getRef().child("readStatus").setValue(1);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }
        });


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.chat));
    }
}
