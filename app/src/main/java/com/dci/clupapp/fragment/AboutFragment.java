package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.AboutUsResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends BaseFragment {

    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    AboutUsResponse aboutUsResponse;

    @BindView(R.id.desc)
    TextView mDescription;
    @BindView(R.id.address)
    TextView mAddress;
    @BindView(R.id.mail)
    TextView mEmail;
    @BindView(R.id.phone)
    TextView mPhone;
    ImageView image;
    Unbinder unbinder;

    public AboutFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about, container, false);
        unbinder = ButterKnife.bind(view);
        mDescription = view.findViewById(R.id.desc);
        mAddress = view.findViewById(R.id.address);
        mEmail = view.findViewById(R.id.mail);
        mPhone = view.findViewById(R.id.phone);
        image = view.findViewById(R.id.image);

        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        aboutusAPI();

        return view;
    }

    public void aboutusAPI() {
        showProgress();
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String appversion = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));


        clubAPI.aboutUs(userid, fcm_key, deviceid, deviceos, "android", deviceid, appversion)
                .enqueue(new Callback<AboutUsResponse>() {
                    @Override
                    public void onResponse(Call<AboutUsResponse> call, Response<AboutUsResponse> response) {

                        try {
                            hideProgress();
                            if (response.body() != null) {
                                Log.e("response", ">>" + response.body());
                                aboutUsResponse = response.body();
                                if (aboutUsResponse.getStatus().equals("Success")) {

                                    mEmail.setText(aboutUsResponse.getResults().get(0).getMailid());
                                    mAddress.setText(aboutUsResponse.getResults().get(0).getAddress());
                                    mPhone.setText(aboutUsResponse.getResults().get(0).getContact());
                                    mDescription.setText(aboutUsResponse.getResults().get(0).getDescription());
                                    Picasso.get().load(R.drawable.ic_place_holder).into(image);

                                } else if (aboutUsResponse.getStatus().equals("Failed")) {
                                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<AboutUsResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });


    }
    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.about));
    }

}
