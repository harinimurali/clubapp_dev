package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.MediaPlayerActivity;
import com.dci.clupapp.adapter.MusicPlayerAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.MusicPlayerDTO;
import com.dci.clupapp.models.MusicResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class MusicPlayerFragment extends BaseFragment implements Runnable {


    private RecyclerView musicListRecyclerView;
    private MusicPlayerAdapter musicListAdapter;
   // MediaPlayer mediaPlayer = new MediaPlayer();
    MediaPlayer mediaPlayer = new MediaPlayer();
    SeekBar seekBar;
    boolean wasPlaying = false;


    public static final String URL = "http://programmerguru.com/android-tutorial/wp-content/uploads/2013/04/hosannatelugu.mp3";

    private MediaPlayer mediaplayer;
    private TextView seekBarHint;
    private FloatingActionButton play;
    private int totalTime;
    private MediaPlayer mp;
    private Button playBtn;
    List<MusicPlayerDTO> musicList;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    private MusicResponse musicResponse;
    public final static String AUDIO_URL = "audio_url";
    public final static String IMG_URL = "image_url";


    public MusicPlayerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_music_player, container, false);

        final TextView seekBarHint = view.findViewById(R.id.textView);

        seekBar = view.findViewById(R.id.seekBar);
        play = view.findViewById(R.id.button);
        musicListRecyclerView = view.findViewById(R.id.music_player_recyclerview);


        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();

        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.DEVICEOS, Build.VERSION.SDK_INT).apply();

        MusicAPI();

       /* musicListRecyclerView=view.findViewById(R.id.music_player_recyclerview);
        musicList = new ArrayList<>();

        MusicData();
        musicListAdapter = new MusicPlayerAdapter(musicList,getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        musicListRecyclerView.setLayoutManager(mLayoutManager);
        musicListRecyclerView.setItemAnimator(new DefaultItemAnimator());
        musicListRecyclerView.setAdapter(musicListAdapter);
*/

//        musicListAdapter.setOnClickListen(new MusicPlayerAdapter.AddTouchListen() {
//            @Override
//            public void onTouchClick(int position) {
//
//                Intent intent=new Intent(getActivity(), MusicActivity.class);
//                startActivity(intent);
//            }
//        });
        return view;
   }

    private void MusicAPI() {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String page = String.valueOf(sharedPreferences.getInt(Constants.PAGE, 0));
        String pagesize = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String device_imei = String.valueOf(fcmSharedPrefrences.getInt(Constants.DEVICEIMEI,0));
        String device_name = fcmSharedPrefrences.getString(Constants.DEVICENAME, "");

        clubAPI.audioList(userid,deviceid,deviceos,appversion,device_imei,device_name,"1","100",fcm_key).
                enqueue(new Callback<MusicResponse>() {


                    @Override
                    public void onResponse(Call<MusicResponse> call, Response<MusicResponse> response) {

                        try {
                           hideProgress();
                            if (response.body() != null) {
                                musicResponse = response.body();
                                Log.e("response", ">>" + response.body());
                                if (musicResponse.getStatus().equals("Success")) {

                                    final List<MusicResponse.MusicResults> musicRespons = new ArrayList<>();
                                    musicRespons.add(response.body().getResults());

                                    musicListAdapter = new MusicPlayerAdapter(musicRespons);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    musicListRecyclerView.setLayoutManager(mLayoutManager);
                                    musicListRecyclerView.setItemAnimator(new DefaultItemAnimator());
                                    musicListRecyclerView.setAdapter(musicListAdapter);

                                    musicListAdapter.setOnClickListen(new MusicPlayerAdapter.AddTouchListen() {
                                        @Override
                                        public void onTouchClick(int position) {

                                          //  Toast.makeText(getActivity(),"hello",Toast.LENGTH_SHORT).show();
//                                            Intent intent=new Intent(getActivity(),MusicActivity.class);
//                                            startActivity(intent);

                                            Intent intent = new Intent(getActivity(), MediaPlayerActivity.class);
                                        //    intent.putExtra(AUDIO_URL, "http://clubapp.dci.in/public/uploads/video/054727a2002011001-e02-16kHz.wav");
                                        //    intent.putExtra(IMG_URL, "https://dl.dropboxusercontent.com/u/2763264/RSS%20MP3%20Player/img3.jpg");
                                            startActivity(intent);
                                        }
                                    });


                                }


                            }
                        } catch (Exception e) {
                          //  showProgress();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), "Try again..", Toast.LENGTH_SHORT).show();
                        }

                    }


                    @Override
                    public void onFailure(Call<MusicResponse> call, Throwable t) {
                          hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });
    }
public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();


        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }

            seekBar.setProgress(currentPosition);

        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        clearMediaPlayer();
    }

    public void clearMediaPlayer() {
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }
}
