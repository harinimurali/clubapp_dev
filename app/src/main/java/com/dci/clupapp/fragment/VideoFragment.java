package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.GalleryDetailActivity;
import com.dci.clupapp.adapter.GalleryAdapter;
import com.dci.clupapp.adapter.VideoAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.VideoDTO;
import com.dci.clupapp.models.VideoResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;



/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends BaseFragment {


    private RecyclerView videorecyclerview;
    private VideoAdapter videoAdapter;
    private List<VideoResponse.VideoResults> videoList=new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    @Inject
    public ClubAPI clubAPI;
    public VideoResponse videoResponse;

    public VideoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_video, container, false);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();



        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getActivity().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();
        videorecyclerview = (RecyclerView)view.findViewById(R.id.video_recyclerview);
        VideoData();

      /*  mAdapter = new VideoAdapter(videoList,getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);*/


        //  GalleryData();



        return view;
    }

    private void VideoData() {
        String userid = sharedPreferences.getString(Constants.USERID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String page = String.valueOf(fcmSharedPrefrences.getInt(Constants.PAGE,0));
        String pagesize = String.valueOf(fcmSharedPrefrences.getInt(Constants.PAGESIZE,0));

        clubAPI.videoList("1",deviceid,deviceos,fcm_key,appversion,"1","100").
                enqueue(new Callback<VideoResponse>() {


                    @Override
                    public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {


                        try {
                            hideProgress();
                            if (response.body() != null) {
                                videoResponse = response.body();
                                Log.e("response", ">>" + response.body());
                                if (videoResponse.getStatus().equals("Success")) {

                                    final List<VideoResponse.VideoResults.VideoDataItem> videoRespons;
                                    videoRespons=  videoResponse.getResults().getData();

                                    videoAdapter = new VideoAdapter(videoRespons);

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                    videorecyclerview.setLayoutManager(mLayoutManager);

                                    videorecyclerview.setItemAnimator(new DefaultItemAnimator());

                                    videorecyclerview.setAdapter(videoAdapter);


/*
                                    galleryAdapter.setOnClickListen(new GalleryAdapter.AddTouchListen() {
                                        @Override
                                        public void onTouchClick(int position) {
                                            Bundle bundle=new Bundle();
                                            bundle.putString("name",galleryRespons.get(0).getName());
                                            bundle.putString("decription",galleryRespons.get(0).getDescription());
                                            bundle.putString("date",galleryRespons.get(0).getUpdatedAt());
                                            Intent intent=new Intent(getActivity(),GalleryDetailActivity.class);
                                            intent.putExtra("data",bundle);
                                            startActivity(intent);
                                        }
                                    });
*/

                                }


                            }
                        } catch (Exception e) {
                            showProgress();
                            e.printStackTrace();
                            //  startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<VideoResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getActivity(),getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    public void onBackPressed() {
       /* if (Jzvd.backPress()) {
            return;
        }*/
        super.getActivity().onBackPressed();
    }
    @Override
    public void onPause() {
        super.onPause();
        //  Jzvd.releaseAllVideos();
    }


/*
    private void GalleryData() {
        VideoDTO s=new VideoDTO();
        s.setURL("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        s.setStatus("3 days ago");
        s.setTime("12:36 min");
        s.setViews("12k views");
        videoList.add(s);

        s=new VideoDTO();
        s.setURL("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        s.setStatus("10 days ago");
        s.setTime("10:36 min");
        s.setViews("122k views");
        videoList.add(s);


        s=new VideoDTO();
        s.setURL("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        s.setStatus("5 days ago");
        s.setTime("02:36 min");
        s.setViews("6k views");
        videoList.add(s);
    }
*/

}