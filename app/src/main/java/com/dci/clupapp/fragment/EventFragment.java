package com.dci.clupapp.fragment;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.EventDetailActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.EventAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.EventResponse;

import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private EventAdapter eventAdapter;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    CommonResponse commonResponse;
    public EventResponse eventResponse;


    public EventFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_event, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.events_recycle);


        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
//        SharedPreferences sharedPreferences = PreferenceManager
//                .getDefaultSharedPreferences(getActivity());
        //   eventAdapter = new EventAdapter(eventResponse);


/*
        eventAdapter.setOnClickListen(new EventAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Intent intent=new Intent(getActivity(), EventDetailActivity.class);
                startActivity(intent);
            }
        });
*/


        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.DEVICEOS, Build.VERSION.SDK_INT).apply();

        EventAPI();
        return view;
    }

    public void EventAPI() {

        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String page = String.valueOf(sharedPreferences.getInt(Constants.PAGE, 0));
        String pagesize = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.eventList(userid, deviceid, fcm_key, deviceos, appversion, page, pagesize).enqueue(new Callback<EventResponse>() {


            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                try {
                    hideProgress();
                    if (response.body() != null) {
                        eventResponse = response.body();
                        Log.e("response", ">>" + response.body());
                        if (eventResponse.getStatus().equals("Success")) {

                            final List<EventResponse.EventResults> eventResponse = new ArrayList<>();
                            eventResponse.add(response.body().getResults());

                            eventAdapter = new EventAdapter(eventResponse, getActivity());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                            recyclerView.setLayoutManager(mLayoutManager);

                            recyclerView.setItemAnimator(new DefaultItemAnimator());

                            recyclerView.setAdapter(eventAdapter);

                            eventAdapter.setOnClickListen(new EventAdapter.AddTouchListen() {
                                @Override
                                public void onTouchClick(int position) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("name", eventResponse.get(0).getData().get(position).getName());
                                    bundle.putString("eventId", String.valueOf(eventResponse.get(0).getData().get(position).getId()));
                                    bundle.putString("decription", eventResponse.get(0).getData().get(position).getDescription());
                                    bundle.putString("date", eventResponse.get(0).getData().get(position).getUpdatedAt());
                                    bundle.putString("location", eventResponse.get(0).getData().get(position).getVendor().getAddress2());
                                    bundle.putString("image", eventResponse.get(0).getData().get(position).getImage());
                                    bundle.putString("status", String.valueOf(eventResponse.get(0).getData().get(position).getStatus()));
                                    Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                                    intent.putExtra("type", bundle);
                                    startActivity(intent);
                                }

                                @Override
                                public void onTouchAcceptClick(int position) {
                                    String id = String.valueOf(eventResponse.get(0).getData().get(position).getId());

                                    EventAccept(id);
                                }
                            });
                        } else {
                           // startActivity(new Intent(getActivity(), MainActivity.class));
                            Toast.makeText(getActivity(), "Try again..", Toast.LENGTH_SHORT).show();
                        }

                        // isUserExists = response.body();
                   /* if (isUserExists.status == Util.STATUS_SUCCESS) {
                        editor.putInt(Constants.USERTYPE, isUserExists.getUserExist());
                        editor.putString(Constants.PHONE, isUserExists.getPhoneNumber());
                        editor.putString(Constants.SECURITYPIN, isUserExists.getSecurityPin());
//                        editor.putString(Constants.FNAME,isUserExists.getData().get(0).getFname());
//                        editor.putString(Constants.LNAME,isUserExists.getData().get(0).getLname());
//                        editor.putString(Constants.SALUTATION,isUserExists.getData().get(0).getSalutation());
//                        editor.putString(Constants.PHONE,isUserExists.getData().get(0).getPhone());
//                        editor.putString(Constants.EMAIL,isUserExists.getData().get(0).getEmail());
                        editor.commit();
                        if (isUserExists.getSecurityPin().length() > 3) {
                            startActivity(new Intent(getActivity(), SecurityPinActivity.class));
                            getActivity().finish();
                        } else {
                            startActivity(new Intent(getActivity(), OtpverificationActivity.class).putExtra("phn_number", mPhone_number.getText().toString()));
                            getActivity().finish();
                        }

                    } else {
                        Toast.makeText(getContext(), isUserExists.getMessage(), Toast.LENGTH_SHORT).show();
                    }*/
                    } else {
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    startActivity(new Intent(getActivity(), MainActivity.class));
                }

            }


            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {
                //  hideProgress();
                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void EventAccept(String eventid) {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String appversion = String.valueOf(sharedPreferences.getInt(Constants.PAGESIZE, 0));

        clubAPI.acceptList(userid, eventid, deviceid, fcm_key, deviceos, appversion)
                .enqueue(new Callback<CommonResponse>() {


                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {

                        try {
                            //  hideProgress();
                            if (response.body() != null) {
                                Log.e("response", ">>" + response.body());
                                if (commonResponse.getStatus().equals("Success")) {
                                    Toast.makeText(getActivity(), "Updated Successfully", Toast.LENGTH_SHORT).show();
                                    eventAdapter.notifyDataSetChanged();

                                } else if (commonResponse.getStatus().equals("fail")) {
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                                }


                            } else {
                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            startActivity(new Intent(getActivity(), MainActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }


/*
    private void EventData() {


            EventDTO s = new EventDTO();
            s.setImg(String.valueOf(R.mipmap.person));
            s.setTitle("Bussiness Meeting");
            s.setLocation_txt("Los Vegas");
            s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
            s.setDate("15 Sep 2018");
            s.setTime("2.30 Pm");
           eventList.add(s);


        s = new EventDTO();
        s.setImg(String.valueOf(R.mipmap.person));
        s.setTitle("Bussiness Meeting");
        s.setLocation_txt("Los Vegas");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30 Pm");
        eventList.add(s);

        s = new EventDTO();
        s.setImg(String.valueOf(R.mipmap.person));
        s.setTitle("Bussiness Meeting");
        s.setLocation_txt("Los Vegas");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30 Pm");
        eventList.add(s);

        s = new EventDTO();
        s.setImg(String.valueOf(R.mipmap.person));
        s.setTitle("Bussiness Meeting");
        s.setLocation_txt("Los Vegas");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30 Pm");
        eventList.add(s);

        s = new EventDTO();
        s.setImg(String.valueOf(R.mipmap.person));
        s.setTitle("Bussiness Meeting");
        s.setLocation_txt("Los Vegas");
        s.setDesc("Principal Burnley High School is fortunate to have a great PTO at her school.The PTO parents");
        s.setDate("15 Sep 2018");
        s.setTime("2.30 Pm");
        eventList.add(s);

    }
*/

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.events));
    }
}
