package com.dci.clupapp.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.FriendListActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.activity.MemberDetailActivity;
import com.dci.clupapp.activity.ProjectDetailActivity;
import com.dci.clupapp.adapter.ContactListAdapter;
import com.dci.clupapp.adapter.MemberlistAdapter;
import com.dci.clupapp.adapter.ProjectlistAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ChatMessageModel;
import com.dci.clupapp.models.MemberResponse;
import com.dci.clupapp.models.MembersDTO;
import com.dci.clupapp.models.ProjectResponse;
import com.dci.clupapp.models.ResultsItem;
import com.dci.clupapp.models.SearchMemberResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by harini on 11/8/2018.
 */

public class MemberlistFragment extends BaseFragment {
    @BindView(R.id.memberlist_recycle)
    RecyclerView memberlistRecycle;
    @BindView(R.id.member_search)
    SearchView searchView;
    MemberlistAdapter memberlistAdapter;
    List<MemberResponse> memberResponseList;
    List<SearchMemberResponse.ResultsItem> searchMemberResponseList;
    Unbinder unbinder;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    private SharedPreferences fcmSharedPrefrences;
    MemberResponse memberResponse;
    List<MemberResponse.memberData> dataItems;
    MemberResponse.MemberRes memberData;
    List<MemberResponse.memberData> resultsItems;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_memberlist, container, false);
        unbinder = ButterKnife.bind(view);
        ButterKnife.bind(this, view);
        searchView.setActivated(true);
        searchView.setQueryHint("Search Member");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);

        final EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.text_subtitle_color));

        //  Predata();
        memberListAPI();

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = searchEditText.getText().toString().toLowerCase(Locale.getDefault());
                getsearch(text);
            }
        });
       /* memberResponse = new MemberlistAdapter(membersDTOS);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        memberlistRecycle.setLayoutManager(mLayoutManager);
        memberlistRecycle.setItemAnimator(new DefaultItemAnimator());
        memberlistRecycle.setAdapter(memberlistAdapter);
        memberlistAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                startActivity(new Intent(getActivity(), MemberDetailActivity.class));
            }
        });*/
        return view;
    }

    public void memberListAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.memberList(userid, deviceid, appid, osversion, appversion, "1", "100").
                enqueue(new Callback<MemberResponse>() {
                    @Override
                    public void onResponse(Call<MemberResponse> call, Response<MemberResponse> response) {

                        try {
                            hideProgress();
                            if (response.body() != null) {
                                memberResponse = response.body();
                                memberResponseList = new ArrayList<>();
                                memberResponseList.add(memberResponse);
                                dataItems = new ArrayList<>();
                                dataItems = memberResponseList.get(0).getResults().getData();
                                if (memberResponse.getStatus().equalsIgnoreCase("Success")) {
                                    memberlistAdapter = new MemberlistAdapter(dataItems, getActivity());
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    memberlistRecycle.setLayoutManager(mLayoutManager);
                                    memberlistRecycle.setItemAnimator(new DefaultItemAnimator());
                                    memberlistRecycle.setAdapter(memberlistAdapter);
                                    memberlistAdapter.setOnClickListen(new MemberlistAdapter.AddTouchListener() {
                                        @Override
                                        public void onTouchClick(int position) {
                                            Bundle bundle = new Bundle();

                                            bundle.putString("id", String.valueOf(dataItems.get(position).getId()));
                                            bundle.putString("name", dataItems.get(position).getFirstname());
                                            bundle.putString("email", String.valueOf(dataItems.get(position).getEmail()));
                                            bundle.putString("image", dataItems.get(position).getProfileImage());
                                            bundle.putString("position", dataItems.get(position).getPosition().getName());
                                            bundle.putString("classification", dataItems.get(position).getClassification().getName());
                                            bundle.putString("city", dataItems.get(position).getCity().getName());
                                            bundle.putString("mobile", dataItems.get(position).getMobile());
                                            bundle.putString("dob", dataItems.get(position).getDob());
                                            bundle.putString("address", dataItems.get(position).getAddress1());
                                            /*Bundle args = new Bundle();
                                            args.putSerializable("arrayObject", (Serializable) dataItems.get(position));
                                            MemberResponse.memberData memberData =dataItems.get(position);*/

                                            startActivity(new Intent(getActivity(), MemberDetailActivity.class).putExtra("data", bundle));
                                        }

                                        @Override
                                        public void onTouchCall(int position) {
                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + dataItems.get(position).getMobile()));
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onTouchChat(int position) {

                                        }
                                    });
                                } else {
                                    Toast.makeText(getActivity(), memberResponse.getError().toString(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<MemberResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void getsearch(String text) {
       // showProgress();
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String vendorid = sharedPreferences.getString(Constants.USERID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.searchMember(userid, vendorid, fcm_key, deviceid, osversion, appversion, "moto", "48956156156121", text).
                enqueue(new Callback<MemberResponse.MemberRes>() {
                    @Override
                    public void onResponse(Call<MemberResponse.MemberRes> call, Response<MemberResponse.MemberRes> response) {
                        //hideProgress();
                        memberData = response.body();
                        resultsItems = new ArrayList<>();


                        if (memberData.getStatus().equalsIgnoreCase("Success")) {
                            resultsItems = memberData.getData();
                            memberlistAdapter = new MemberlistAdapter(resultsItems, getActivity());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                            memberlistRecycle.setLayoutManager(mLayoutManager);
                            memberlistRecycle.setItemAnimator(new DefaultItemAnimator());
                            memberlistRecycle.setAdapter(memberlistAdapter);
                            memberlistAdapter.setOnClickListen(new MemberlistAdapter.AddTouchListener() {
                                @Override
                                public void onTouchClick(int position) {
                                    Bundle bundle = new Bundle();

                                    bundle.putString("id", String.valueOf(resultsItems.get(position).getId()));
                                    bundle.putString("name", resultsItems.get(position).getFirstname());
                                    bundle.putString("email", String.valueOf(resultsItems.get(position).getEmail()));
                                    bundle.putString("image", resultsItems.get(position).getProfileImage());
                                    bundle.putString("position", resultsItems.get(position).getPosition().getName());
                                    bundle.putString("classification", resultsItems.get(position).getClassification().getName());
                                    bundle.putString("city", resultsItems.get(position).getCity().getName());
                                    bundle.putString("mobile", resultsItems.get(position).getMobile());
                                    bundle.putString("dob", resultsItems.get(position).getDob());
                                    bundle.putString("address", resultsItems.get(position).getAddress1());
                                            /*Bundle args = new Bundle();
                                            args.putSerializable("arrayObject", (Serializable) dataItems.get(position));
                                            MemberResponse.memberData memberData =dataItems.get(position);*/

                                    startActivity(new Intent(getActivity(), MemberDetailActivity.class).putExtra("data", bundle));
                                }

                                @Override
                                public void onTouchCall(int position) {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + dataItems.get(position).getMobile()));
                                    startActivity(intent);
                                }

                                @Override
                                public void onTouchChat(int position) {

                                }
                            });
                        } else {
                            memberlistRecycle.setVisibility(View.GONE);
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                        }

                    }


                    @Override
                    public void onFailure(Call<MemberResponse.MemberRes> call, Throwable t) {
                      //  hideProgress();
                        memberlistRecycle.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), "server error", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    /*
        private void Predata() {
            MembersDTO membersDTO = new MembersDTO();
            membersDTO.setImage(String.valueOf(R.mipmap.person));
            membersDTO.setName("Vincent Martin");
            membersDTO.setPosition("Member");
            membersDTO.setMobilenumber("+91 9876543456");
            membersDTOS.add(membersDTO);

            membersDTO = new MembersDTO();
            membersDTO.setImage(String.valueOf(R.mipmap.person));
            membersDTO.setName("John Mckey");
            membersDTO.setPosition("Vice President");
            membersDTO.setMobilenumber("+91 8765432197");
            membersDTOS.add(membersDTO);


            membersDTO = new MembersDTO();
            membersDTO.setImage(String.valueOf(R.mipmap.person));
            membersDTO.setName("George");
            membersDTO.setPosition("Member");
            membersDTO.setMobilenumber("+91 9876543223");
            membersDTOS.add(membersDTO);


            membersDTO = new MembersDTO();
            membersDTO.setImage(String.valueOf(R.mipmap.person));
            membersDTO.setName("John Mckey");
            membersDTO.setPosition("Member");
            membersDTO.setMobilenumber("+91 8970564321");
            membersDTOS.add(membersDTO);

            membersDTO = new MembersDTO();
            membersDTO.setImage(String.valueOf(R.mipmap.person));
            membersDTO.setName("George");
            membersDTO.setPosition("Member");
            membersDTO.setMobilenumber("+91 9087654333");
            membersDTOS.add(membersDTO);
        }
    */
    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.member_directory));
    }
}
