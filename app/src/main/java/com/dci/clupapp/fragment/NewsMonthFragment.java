package com.dci.clupapp.fragment;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.NewsDetailActivity;
import com.dci.clupapp.adapter.NewsMonthAdapter;
import com.dci.clupapp.adapter.NewsTodayAdapter;
import com.dci.clupapp.models.NewsResponse;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.AppPreferences;

import java.util.List;

import static com.dci.clupapp.fragment.BaseFragment.getTimeFormat;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class NewsMonthFragment extends Fragment {
    private RecyclerView newsListRecycle;
    private NewsMonthAdapter newsListAdapter;
    private List<NewsResponse.Results.MonthItem> newsList;
    private TextView no_data;


    @SuppressLint("ValidFragment")
    public NewsMonthFragment(List<NewsResponse.Results.MonthItem> month) {
        this.newsList = month;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_today, container, false);
        newsListRecycle = view.findViewById(R.id.news_today_recyclerview);
        no_data=view.findViewById(R.id.no_data);

        if(newsList.size()==0){
            newsListRecycle.setVisibility(View.GONE);
            no_data.setVisibility(View.VISIBLE);
        }

        else{

        newsListAdapter = new NewsMonthAdapter(getActivity(), newsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        newsListRecycle.setLayoutManager(mLayoutManager);
        newsListRecycle.setItemAnimator(new DefaultItemAnimator());
        newsListRecycle.setAdapter(newsListAdapter);

        newsListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {

                Bundle bundle = new Bundle();
                bundle.putString("id",String.valueOf(newsList.get(position).getId()));
                bundle.putString("title", String.valueOf(newsList.get(position).getTitle()));
                bundle.putString("description", String.valueOf(newsList.get(position).getContent()));
                bundle.putString("post", AppPreferences.getProfile(getContext()).get(position).getResults().getFirstname());
                bundle.putString("image", String.valueOf(newsList.get(position).getImage()));
                bundle.putString("date", getTimeFormat(String.valueOf(newsList.get(position).getUpdatedAt())));
               // holder.duration.setText(getTimeFormat(list.getUpdatedAt()));

                Intent intent=new Intent(getActivity(), NewsDetailActivity.class);
                intent.putExtra("data",bundle);
                startActivity(intent);
            }
        });
        }
        return view;

    }

}
