package com.dci.clupapp.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.LoginActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.activity.ProjectDetailActivity;
import com.dci.clupapp.adapter.ProjectlistAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.ProjectDTO;
import com.dci.clupapp.models.ProjectResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by harini on 11/19/2018.
 */

public class ProjectListFragment extends BaseFragment {
    @BindView(R.id.projectlist_recycle)
    RecyclerView mprojectlistRecycle;
    @BindView(R.id.project_search)
    SearchView searchView;
    ProjectlistAdapter projectlistAdapter;
    List<ProjectResponse> projectResponsObj;
    List<ProjectResponse.DataRes> dataItems;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    private SharedPreferences fcmSharedPrefrences;

    ProjectResponse projectResponse;
    public ProjectListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_projectlist, container, false);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        ButterKnife.bind(this, view);
        searchView.setActivated(true);
        searchView.setQueryHint("Search");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();

        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.text_subtitle_color));

        // Projectlist();
        projectListAPI();
        return view;
    }

    public void projectListAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.projectList(userid, deviceid, appid, osversion, appversion, "1", "1").
                enqueue(new Callback<ProjectResponse>() {
                    @Override
                    public void onResponse(Call<ProjectResponse> call, Response<ProjectResponse> response) {
                        try {
                            hideProgress();
                            if (response.body() != null) {
                                projectResponse = response.body();
                                projectResponsObj = new ArrayList<>();
                                projectResponsObj.add(projectResponse);
                                dataItems = new ArrayList<>();
                                dataItems = projectResponsObj.get(0).getResults().getData();
                                if (projectResponse.getStatus().equalsIgnoreCase("Success")) {
                                    projectlistAdapter = new ProjectlistAdapter(dataItems, getActivity());
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    mprojectlistRecycle.setLayoutManager(mLayoutManager);
                                    mprojectlistRecycle.setItemAnimator(new DefaultItemAnimator());
                                    mprojectlistRecycle.setAdapter(projectlistAdapter);
                                    projectlistAdapter.setOnClickListen(new AddTouchListen() {
                                        @Override
                                        public void onTouchClick(int position) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("name", dataItems.get(position).getName());
                                            bundle.putString("eventid", String.valueOf(dataItems.get(position).getId()));
                                            bundle.putString("image", dataItems.get(position).getImage());
                                            bundle.putString("description", dataItems.get(position).getDescription());
                                            bundle.putString("date", dataItems.get(position).getUpdatedAt());
                                            bundle.putString("location", dataItems.get(position).getVenue());
                                            startActivity(new Intent(getActivity(), ProjectDetailActivity.class).putExtra("data", bundle));
                                        }
                                    });
                                } else {
                                    Toast.makeText(getActivity(), projectResponse.getError().toString(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<ProjectResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }

/*
    private void Projectlist() {


        ProjectDTO s = new ProjectDTO();
        // s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);

        s = new ProjectDTO();
        //  s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);

        s = new ProjectDTO();
        // s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);

        s = new ProjectDTO();
        // s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);

        s = new ProjectDTO();
        // s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);

        s = new ProjectDTO();
        // s.setImage(String.valueOf(R.mipmap.bussimg));
        s.setTitle("Village Project");
        s.setLocation("Los Vegas");
        s.setDescription("Lorem Ipsium dolar sit amet, consecteur adipiscising edit so, sed so edisumud temper incident at dolar malquinous");
        s.setDate("15 Sep 2018");
        projectDTOS.add(s);


    }
*/
@Override
public void onResume() {
    super.onResume();
    MainActivity.title.setText(getActivity().getResources().getString(R.string.project));
}

}
