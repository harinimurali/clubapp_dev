package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.GalleryDetailActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.GalleryAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.GalleryResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryFragment extends Fragment {


    private RecyclerView GalleryRecyclerView;
    private GalleryAdapter galleryAdapter;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    @Inject
    public ClubAPI clubAPI;
    public GalleryResponse galleryResponse;

    public GalleryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery, container, false);


        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();


        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();

        GalleryRecyclerView = (RecyclerView) view.findViewById(R.id.gallery_recycleview);
        GalleryAPI();


      /*  mAdapter = new GalleryAdapter(galleryList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(mAdapter);
*/



       /* mAdapter.setOnClickListen(new GalleryAdapter.AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                Intent intent=new Intent(getActivity(), GalleryDetailActivity.class);
                startActivity(intent);
            }
        });*/

        return view;
    }

    private void GalleryAPI() {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");


        clubAPI.galleryList(userid, deviceid, fcm_key, deviceos, appversion).
                enqueue(new Callback<GalleryResponse>() {

                    @Override
                    public void onResponse(Call<GalleryResponse> call, Response<GalleryResponse> response) {
                        try {
                            //  hideProgress();
                            if (response.body() != null) {
                                galleryResponse = response.body();
                                Log.e("response", ">>" + response.body());
                                if (galleryResponse.getStatus().equals("Success")) {

                                    final List<GalleryResponse.GalleryResult> galleryRespons;
/*
List<GalleryResponse.GalleryResult.GalleryAttachment>  galleryAttachments = new ArrayList<>();

                                    final ArrayList<GalleryResponse.GalleryResult> galleryResponsArray;
                                    for (int i = 0; i < galleryResponse.getResults().size(); i++) {
                                        if(galleryResponse.getResults().get(i).getAttachment().size()!=0){
                                            GalleryResponse attachment = new GalleryResponse();
                                            galleryAttachments.add(attachment.getResults().get(i).getAttachment().get());
                                        }
                                    }
*/
                                    galleryRespons = galleryResponse.getResults();


                                    galleryAdapter = new GalleryAdapter(galleryRespons);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                    GalleryRecyclerView.setLayoutManager(mLayoutManager);

                                    GalleryRecyclerView.setItemAnimator(new DefaultItemAnimator());

                                    GalleryRecyclerView.setAdapter(galleryAdapter);


                                    galleryAdapter.setOnClickListen(new GalleryAdapter.AddTouchListen() {
                                        @Override
                                        public void onTouchClick(int position) {

                                            List<GalleryResponse.GalleryResult.GalleryAttachment> galleryResponsp = new ArrayList<>();
                                            galleryResponsp = galleryRespons.get(position).getAttachment();
                                            ArrayList<String> mylist = new ArrayList<String>();

                                            for (int i = 0; i < galleryResponsp.size(); i++) {
                                                mylist.add(Constants.ImageUrl+galleryResponsp.get(i).getFilePath().trim());

                                            }
                                           /* Intent intent = new Intent(getActivity(), ImageGalleryActivity.class);

                                         //   String[] images = mylist;
                                            Bundle bundle = new Bundle();
                                            bundle.putStringArrayList(ImageGalleryActivity.KEY_IMAGES, new ArrayList<>(mylist));
                                            bundle.putString(ImageGalleryActivity.KEY_TITLE, "Unsplash Images");
                                            intent.putExtras(bundle);

                                            startActivity(intent);*/
                                            Intent intent = new Intent(getActivity(), GalleryDetailActivity.class);
                                            intent.putStringArrayListExtra("data", mylist);
                                            startActivity(intent);

                                        }
                                    });

                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            //  startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<GalleryResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });


    }

/*
    private void GalleryData() {
        GalleryDTO s=new GalleryDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        s.setEvent("New Year Celebration Photos");
        s.setAlbum_count("59 photos,1 video");
        galleryList.add(s);

        s=new GalleryDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        s.setEvent("Annual Year Meeting");
        s.setAlbum_count("39 photos,4 video");
        galleryList.add(s);

        s=new GalleryDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        s.setEvent("May day special photos");
        s.setAlbum_count("19 photos,4 video");
        galleryList.add(s);

        s=new GalleryDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        s.setEvent("Conference hall meeting");
        s.setAlbum_count("9 photos,10 video");
        galleryList.add(s);

        s=new GalleryDTO();
        s.setImg(String.valueOf(R.mipmap.funimage));
        s.setEvent("Bussiness meeting 2017");
        s.setAlbum_count("6 photos,3 video");
        galleryList.add(s);
    }
*/
@Override
public void onResume() {
    super.onResume();
    MainActivity.title.setText(getActivity().getResources().getString(R.string.gallery));
}

}
