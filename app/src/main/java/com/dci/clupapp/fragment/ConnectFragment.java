package com.dci.clupapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.MemberDetailActivity;
import com.dci.clupapp.adapter.ConnectListAdapter;
import com.dci.clupapp.adapter.MemberlistAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ConnectResponse;
import com.dci.clupapp.models.MemberResponse;
import com.dci.clupapp.models.MembersDTO;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by harini on 11/20/2018.
 */

public class ConnectFragment extends BaseFragment implements LocationListener {
    @BindView(R.id.connectlist_recycle)
    RecyclerView connectlistRecycle;
    @BindView(R.id.nearby_search)
    SearchView searchView;
    @BindView(R.id.no_data)
    TextView no_data;
    ConnectListAdapter connectListAdapter;
    List<ConnectResponse> connectResponseList;
    List<ConnectResponse.ResultsItem> dataItems;
    Unbinder unbinder;
    LocationManager locationManager;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    private SharedPreferences fcmSharedPrefrences;
    ConnectResponse connectResponse;
    double mLatitude, mLongitude;

    public ConnectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect, container, false);

        unbinder = ButterKnife.bind(view);
        ButterKnife.bind(this, view);
        searchView.setActivated(true);
        searchView.setQueryHint("Search Name, Club, City");
        searchView.onActionViewExpanded();
        searchView.setIconified(false);
        searchView.clearFocus();
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);

        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.text_subtitle_color));

        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 5, this);

        } catch (SecurityException e) {
            e.printStackTrace();
        }

        ConnectAPI();
        //  Predata();
       /* connectListAdapter = new ConnectListAdapter(membersDTOS);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        connectlistRecycle.setLayoutManager(mLayoutManager);
        connectlistRecycle.setItemAnimator(new DefaultItemAnimator());
        connectlistRecycle.setAdapter(connectListAdapter);
        connectListAdapter.setOnClickListen(new AddTouchListen() {
            @Override
            public void onTouchClick(int position) {
                startActivity(new Intent(getActivity(), MemberDetailActivity.class));
            }
        });*/
        return view;
    }

    public void ConnectAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.USERID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.nearBy(userid, deviceid, appid, osversion, appversion,String.valueOf( mLatitude),String.valueOf( mLongitude)).
                enqueue(new Callback<ConnectResponse>() {
                    @Override
                    public void onResponse(Call<ConnectResponse> call, Response<ConnectResponse> response) {

                        try {
                            hideProgress();
                            if (response.body() != null) {
                                connectResponse = response.body();
                                connectResponseList = new ArrayList<>();
                                connectResponseList.add(connectResponse);
                                dataItems = connectResponseList.get(0).getResults();
                                if (connectResponse.getStatus().equalsIgnoreCase("Success")) {
                                    connectListAdapter = new ConnectListAdapter(dataItems, getActivity());
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    connectlistRecycle.setLayoutManager(mLayoutManager);
                                    connectlistRecycle.setItemAnimator(new DefaultItemAnimator());
                                    connectlistRecycle.setAdapter(connectListAdapter);
                                    connectListAdapter.setOnClickListen(new ConnectListAdapter.AddTouchListener() {
                                        @Override
                                        public void onTouchClick(int position) {
                                            Bundle bundle = new Bundle();

                                            bundle.putString("id", String.valueOf(dataItems.get(position).getId()));
                                            bundle.putString("name", dataItems.get(position).getFirstname());
                                            bundle.putString("email", String.valueOf(dataItems.get(position).getEmail()));
                                            bundle.putString("image", dataItems.get(position).getProfileImage());
                                            bundle.putString("position", dataItems.get(position).getPositionHeld());
                                            bundle.putString("classification", dataItems.get(position).getClassification());
                                            bundle.putString("city", dataItems.get(position).getAddress2());
                                            bundle.putString("mobile", dataItems.get(position).getMobile());
                                            bundle.putString("dob", dataItems.get(position).getDob());
                                            bundle.putString("address", dataItems.get(position).getAddress1());
                                            /*Bundle args = new Bundle();
                                            args.putSerializable("arrayObject", (Serializable) dataItems.get(position));
                                            MemberResponse.memberData memberData =dataItems.get(position);*/

                                            startActivity(new Intent(getActivity(), MemberDetailActivity.class).putExtra("data",  bundle));
                                        }

                                        @Override
                                        public void onTouchCall(int position) {
                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + dataItems.get(position).getMobile()));
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void onTouchChat(int position) {

                                        }
                                    });
                                } else {
                                    connectlistRecycle.setVisibility(View.GONE);
                                    no_data.setVisibility(View.VISIBLE);

                                    Toast.makeText(getActivity(), connectResponse.getError().toString(), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                connectlistRecycle.setVisibility(View.GONE);
                                no_data.setVisibility(View.VISIBLE);

                                Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<ConnectResponse> call, Throwable t) {
                          hideProgress();
                        connectlistRecycle.setVisibility(View.GONE);
                        no_data.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onLocationChanged(Location location) {
        mLatitude = location.getLatitude();
        mLongitude = location.getLongitude();

        try {
            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);

            mLatitude = addresses.get(0).getLatitude();
            mLongitude = addresses.get(0).getLongitude();
            /*locationText.setText(locationText.getText() + "\n" + addresses.get(0).getAddressLine(0) + ", " +
                    addresses.get(0).getAddressLine(1) + ", " + addresses.get(0).getAddressLine(2));*/
        } catch (Exception e) {

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Toast.makeText(getActivity(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


   /* private void Predata() {
        MembersDTO membersDTO = new MembersDTO();
        membersDTO.setImage(String.valueOf(R.mipmap.person));
        membersDTO.setName("Vincent Martin");
        membersDTO.setPosition("ABC club of Newyork");
        membersDTO.setLocation("2 km away");
        membersDTOS.add(membersDTO);

        membersDTO = new MembersDTO();
        membersDTO.setImage(String.valueOf(R.mipmap.person));
        membersDTO.setName("John Mckey");
        membersDTO.setPosition("Vice President");
        membersDTO.setLocation("3 km away");
        membersDTOS.add(membersDTO);


        membersDTO = new MembersDTO();
        membersDTO.setImage(String.valueOf(R.mipmap.person));
        membersDTO.setName("George");
        membersDTO.setPosition("Member");
        membersDTO.setLocation("2 km away");
        membersDTOS.add(membersDTO);


        membersDTO = new MembersDTO();
        membersDTO.setImage(String.valueOf(R.mipmap.person));
        membersDTO.setName("John Mckey");
        membersDTO.setPosition("Member");
        membersDTO.setLocation("1 km away");
        membersDTOS.add(membersDTO);

        membersDTO = new MembersDTO();
        membersDTO.setImage(String.valueOf(R.mipmap.person));
        membersDTO.setName("George");
        membersDTO.setPosition("Member");
        membersDTO.setLocation("3 km away");
        membersDTOS.add(membersDTO);
    }*/


}
