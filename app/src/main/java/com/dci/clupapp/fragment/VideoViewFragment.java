package com.dci.clupapp.fragment;


import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.VideoViewAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.VideoResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoViewFragment extends BaseFragment {
    private RecyclerView recyclerView;
    // private Vector<YoutubeVideos> YoutubeVideos = new Vector<YoutubeVideos>();
    ImageView back;
    // List<YoutubeVideos> youtubeVideos=new ArrayList<>();
    private VideoViewAdapter videoAdapter;
    //  private ArrayList<YoutubeVideos> youtubeVideosList=new ArrayList<>();
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    @Inject
    public ClubAPI clubAPI;
    public VideoResponse videoResponse;
    private RecyclerView videorecyclerview;


    public VideoViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video_view, container, false);


        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();


        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getActivity().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();
        videorecyclerview = (RecyclerView) view.findViewById(R.id.view_view_recyclerview);
       /* recyclerView.setHasFixedSize(true);


        youtubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/V3auP0XRp8g\" frameborder=\"0\" allowfullscreen></iframe>") );
        youtubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/p6qVJ1KhHek\" frameborder=\"0\" allowfullscreen></iframe>") );
        youtubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/RPVe3R3STv4\" frameborder=\"0\" allowfullscreen></iframe>") );
        youtubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/TEYN7S81e88\" frameborder=\"0\" allowfullscreen></iframe>") );
        youtubeVideos.add( new YoutubeVideos("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe>") );

        videoAdapter = new VideoViewAdapter(youtubeVideos);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(videoAdapter);*/


        VideoViewAPI();
        return view;
    }

    private void VideoViewAPI() {
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String deviceos = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String page = String.valueOf(fcmSharedPrefrences.getInt(Constants.PAGE, 0));
        String pagesize = String.valueOf(fcmSharedPrefrences.getInt(Constants.PAGESIZE, 0));

        clubAPI.videoList(userid, deviceid, appversion, deviceos, fcm_key, "1", "100").
                enqueue(new Callback<VideoResponse>() {


                    @Override
                    public void onResponse(Call<VideoResponse> call, Response<VideoResponse> response) {


                        try {
                            hideProgress();
                            if (response.body() != null) {
                                videoResponse = response.body();
                                Log.e("response", ">>" + response.body());
                                if (videoResponse.getStatus().equals("Success")) {

                                    final List<VideoResponse.VideoResults.VideoDataItem> videoRespons;
                                    videoRespons = videoResponse.getResults().getData();

                                    videoAdapter = new VideoViewAdapter(videoRespons);

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

                                    videorecyclerview.setLayoutManager(mLayoutManager);

                                    videorecyclerview.setItemAnimator(new DefaultItemAnimator());

                                    videorecyclerview.setAdapter(videoAdapter);

                                }


                            }
                        } catch (Exception e) {
                            showProgress();
                            e.printStackTrace();
                            //  startActivity(new Intent(getApplicationContext(), NotificationActivity.class));
                        }

                    }


                    @Override
                    public void onFailure(Call<VideoResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /*
        public void YoutubeData()
        {
            youtubeVideosList = new ArrayList<>();

            youtubeVideosList.add(
                    new YoutubeVideos(
                            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/V3auP0XRp8g\" frameborder=\"0\" allowfullscreen></iframe>",

                            "Maths",
                            "All Units are covered in this video",
                            "www.mathstutorial.com"


                    ));
            youtubeVideosList.add(
                    new YoutubeVideos(
                            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/p6qVJ1KhHek\" frameborder=\"0\" allowfullscreen></iframe>",

                            "English",
                            "All Units are covered in this video",
                            "www.englishtutorial.com"


                    ));

            youtubeVideosList.add(
                    new YoutubeVideos(
                            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/RPVe3R3STv4\" frameborder=\"0\" allowfullscreen></iframe>",

                            "Tamil",
                            "All Units are covered in this video",
                            "www.tamiltutorial.com"


                    ));

            youtubeVideosList.add(
                    new YoutubeVideos(
                            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/TEYN7S81e88\" frameborder=\"0\" allowfullscreen></iframe>",

                            "Science",
                            "All Units are covered in this video",
                            "www.sciencetutorial.com"


                    ));

            youtubeVideosList.add(
                    new YoutubeVideos(
                            "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/icgcUUu0gNs\" frameborder=\"0\" allowfullscreen></iframe>",

                            "Social",
                            "All Units are covered in this video",
                            "www.socialtutorial.com"


                    ));

        }
    */
    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.video));
    }

}
