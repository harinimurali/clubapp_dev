package com.dci.clupapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.adapter.WebViewClientImpl;

/**
 * A simple {@link Fragment} subclass.
 */
public class WebsiteBlogFragment extends BaseFragment {

    private WebView webView = null;

    public WebsiteBlogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_website_blog, container, false);

        WebView webView = (WebView) view.findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        WebViewClientImpl webViewClient = new WebViewClientImpl(getActivity());
        webView.setWebViewClient(webViewClient);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                showProgress();
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, final String url) {
                hideProgress();
            }
        });
        webView.loadUrl("https://www.dotcominfoway.com");


        return view;


    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }

        return onKeyDown(keyCode, event);
    }
    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.blog));
    }

}
