package com.dci.clupapp.fragment;


import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chaos.view.PinView;
import com.dci.clupapp.BuildConfig;
import com.dci.clupapp.R;
import com.dci.clupapp.activity.BaseActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.ProfileResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.msg91.sendotp.library.SendOtpVerification;
import com.msg91.sendotp.library.Verification;
import com.msg91.sendotp.library.VerificationListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtpVerificationFragment extends BaseFragment implements
        ActivityCompat.OnRequestPermissionsResultCallback, VerificationListener {

    @BindView(R.id.otp_verify)
    ImageView otpVerify;
    @BindView(R.id.otp_pinView)
    PinView otpView;
    @BindView(R.id.resend_otp)
    TextView resendOtp;
    String mobileNumber;

    private static final String TAG = Verification.class.getSimpleName();
    private Verification mVerification;


    private ImageView otp_verify;
    @Inject
    public ClubAPI clubAPI;
    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    private SharedPreferences fcmSharedPrefrences;
    ProfileResponse userProfileObj;
    Unbinder unbinder;

    public OtpVerificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_otp_verification, container, false);
        ClubApplication.getContext().getComponent().inject(this);
        unbinder = ButterKnife.bind(view);
        editor = sharedPreferences.edit();
        Bundle bundle = getArguments();
        if (getArguments() != null) {

            mobileNumber = bundle.getString(Constants.MOBILENUMBER);

        }
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor.putString(Constants.DEVICEID, Settings.Secure.getString(getActivity().getContentResolver(),
                Settings.Secure.ANDROID_ID)).apply();
        editor.putString(Constants.APPID, getContext().getPackageName()).apply();
        editor.putString(Constants.APPVERSION, BuildConfig.VERSION_NAME).apply();
        editor.putInt(Constants.DEVICEOS, Build.VERSION.SDK_INT).apply();
        editor.putInt(Constants.OSVERSION, Build.VERSION.SDK_INT).apply();
        otp_verify = view.findViewById(R.id.otp_verify);
        otpView = view.findViewById(R.id.otp_pinView);
        resendOtp = view.findViewById(R.id.resend_otp);
        initiateVerification();
        new CountDownTimer(30000, 1000) {
            public void onTick(long millisUntilFinished) {
                resendOtp.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                resendOtp.setClickable(false);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                String mystring=getActivity().getResources().getString(R.string.resend_otp);
                SpannableString content = new SpannableString(mystring);
                content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
                resendOtp.setText(content);
              //  resendOtp.setText(getActivity().getResources().getString(R.string.resend_otp));
                resendOtp.setClickable(true);
            }

        }.start();

        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateVerification();
                Toast.makeText(getActivity(), "OTP sent to your number", Toast.LENGTH_SHORT).show();
                new CountDownTimer(30000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        resendOtp.setText("Retry in " + millisUntilFinished / 1000 + " seconds");
                        resendOtp.setClickable(false);
                        //here you can have your logic to set text to edittext
                    }

                    public void onFinish() {
                        String mystring=getActivity().getResources().getString(R.string.resend_otp);
                        SpannableString content = new SpannableString(mystring);
                        content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
                        resendOtp.setText(content);
                        resendOtp.setClickable(true);
                    }

                }.start();

            }
        });

        otp_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (otpView.getText().toString().length() == 0) {
                    otpView.setError("Please Enter OTP");
                } else {
                    onSubmitClicked();
                }
            }
        });

        return view;
    }

    public void onSubmitClicked() {
        String code = otpView.getText().toString();
        if (!code.isEmpty()) {

            if (mVerification != null) {
                mVerification.verify(code);


                showProgress();
               /* TextView messageText = (TextView) findViewById(R.id.textView);
                messageText.setText("Verification in progress");*/
                // enableInputField(false);
            }
        }
    }

    public void otpVerifyAPI() {
        showProgress();
        String appid = sharedPreferences.getString(Constants.APPID, "");
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String mobilenumber = sharedPreferences.getString(Constants.MOBILENUMBER, "");
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");

        clubAPI.userLogin(mobileNumber, fcm_key, deviceid, "android", osversion, osversion, appversion).
                // clubAPI.userLogin("9487292025", "ygjhtdryhdfghdfhdghgfhf", "8568685", "android", "7574574575", "24", "1.0").
                        enqueue(new Callback<ProfileResponse>() {
                    @Override
                    public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                        try {
                            hideProgress();
                            if (response.body() != null) {
                                userProfileObj = response.body();
                                hideProgress();
                                Log.e("response", ">>" + userProfileObj.getStatus());
                                if (userProfileObj.getStatus().equals("Success")) {
                                    hideProgress();
                                    List<ProfileResponse> userProfileObjs = new ArrayList<>();
                                    userProfileObjs.add(userProfileObj);
                                    AppPreferences.setProfile(getActivity(), userProfileObjs);
                                    editor.putString(Constants.USERID, String.valueOf(userProfileObj.getResults().getId()));
                                    editor.putString(Constants.VENDORID, String.valueOf(userProfileObj.getResults().getVendorId()));
                                    editor.putBoolean(Constants.LOGIN_STATUS, true).commit();

                                    AppPreferences.setLoginStatus(getActivity(), true);
                                    // editor.putString(Constants.SECURITYPIN, userProfileObj.getResults());
                                    editor.commit();
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                } else {
                                    hideProgress();
                                    AppPreferences.setLoginStatus(getActivity(), false);
                                    editor.putBoolean(Constants.LOGIN_STATUS, false);
                                    Toast.makeText(getActivity(), "Try again..", Toast.LENGTH_SHORT).show();
                                    hideProgress();
                                }
                            } else {
                                hideProgress();
                                AppPreferences.setLoginStatus(getActivity(), false);
                                editor.putBoolean(Constants.LOGIN_STATUS, false);
                                Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            hideProgress();
                            AppPreferences.setLoginStatus(getActivity(), false);
                            editor.putBoolean(Constants.LOGIN_STATUS, false);
                            Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ProfileResponse> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getContext(), getActivity().getResources().getString(R.string.try_again), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    void createVerification(String phoneNumber, boolean skipPermissionCheck, String countryCode) {
       /* if (!skipPermissionCheck && ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) ==
                PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, 0);
            hideProgress();
        } else {*/
        mVerification = SendOtpVerification.createSmsVerification
                (SendOtpVerification
                        .config(countryCode + phoneNumber)
                        .context(getContext())
                        .autoVerification(true)
                        .build(), this);
        mVerification.initiate();
            /*mVerification = SendOtpVerification.createSmsVerification
                    (SendOtpVerification
                            .config(countryCode + phoneNumber)
                            .context(getActivity())
                            .autoVerification(false)
                            .otplength("4")
                            .expiry("5")
                            .httpsConnection(false)
                            .senderId("Club app")
                            .build(), this);
            mVerification.initiate();*/
        // }
    }


    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0])) {
                Toast.makeText(getActivity(), "This application needs permission to read your SMS to automatically verify your "
                        + "phone, you may disable the permission once you have been verified.", Toast.LENGTH_LONG)
                        .show();
            }
        }
        initiateVerificationAndSuppressPermissionCheck();
    }

    void initiateVerification() {
        initiateVerification(false);
    }

    void initiateVerification(boolean skipPermissionCheck) {
        //set user number in textview
        Bundle bundle = getArguments();
        if (getArguments() != null) {
            mobileNumber = getArguments().getString(Constants.MOBILENUMBER);

            if (mobileNumber != null) {
                createVerification(mobileNumber, skipPermissionCheck, "+91");
            }
        }
    }

    void initiateVerificationAndSuppressPermissionCheck() {
        initiateVerification(true);
    }

    @Override
    public void onInitiated(String response) {

    }

    @Override
    public void onInitiationFailed(Exception paramException) {

    }

    @Override
    public void onVerified(String response) {
        otpVerifyAPI();

    }

    @Override
    public void onVerificationFailed(Exception paramException) {
        Log.e(TAG, "Verification initialization failed: " + paramException.getMessage());
        Toast.makeText(getActivity(), "Verfication Failed", Toast.LENGTH_LONG).show();
        hideProgress();
    }
}
