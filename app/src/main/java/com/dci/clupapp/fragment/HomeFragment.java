package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.dci.clupapp.R;

import com.dci.clupapp.activity.EventDetailActivity;
import com.dci.clupapp.activity.MemberDetailActivity;
import com.dci.clupapp.activity.NewsDetailActivity;
import com.dci.clupapp.activity.ProjectDetailActivity;
import com.dci.clupapp.adapter.SliderAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.BannerObjects;

import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.google.gson.JsonElement;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.clupapp.activity.MainActivity.setTitle;


/**
 * Created by harini on 11/19/2018.
 */

public class HomeFragment extends BaseFragment {
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    private ArrayList<BannerObjects> imageModelArrayList;
    @BindView(R.id.event_layout)
    RelativeLayout evenLayout;
    @BindView(R.id.news_layout)
    RelativeLayout newsLayout;
    @BindView(R.id.news_title)
    TextView mnewsTitle;
    @BindView(R.id.news_desc)
    TextView mnewsDesc;
    @BindView(R.id.connect_layout)
    RelativeLayout connectLayout;
    Fragment fragment;
    CirclePageIndicator indicator;
    Unbinder unbinder;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    private SharedPreferences fcmSharedPrefrences;

    String newsDescription, newsTitle;
    private SliderAdapter sliderAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
      /*  imageModelArrayList = new ArrayList<>();
        imageModelArrayList = populateList();*/
        ButterKnife.bind(view);

        init(view);
        return view;

    }

    private void init(View view) {

        mPager = (ViewPager) view.findViewById(R.id.pager);
        evenLayout = view.findViewById(R.id.event_layout);
        newsLayout = view.findViewById(R.id.news_layout);
        mnewsDesc = view.findViewById(R.id.news_desc);
        mnewsTitle = view.findViewById(R.id.news_title);

        connectLayout = view.findViewById(R.id.connect_layout);

        indicator = (CirclePageIndicator)
                view.findViewById(R.id.indicator);

       /* indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = imageModelArrayList.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });*/
        evenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new EventFragment();
                replaceFragment(fragment);
                setTitle(getResources().getString(R.string.events));
            }
        });
        newsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new NewsFragment();
                replaceFragment(fragment);
                setTitle(getResources().getString(R.string.news));
            }
        });
        connectLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new ConnectFragment();
                replaceFragment(fragment);
                setTitle(getResources().getString(R.string.connect));
            }
        });
        trendingAPI();
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName;
        backStateName = ((Object) fragment).getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back_arrow stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.main, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    public void trendingAPI() {
        showProgress();
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");


        clubAPI.trending(userid, fcm_key, deviceid, "android", deviceid, osversion, appversion).
                enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                        try {
                            hideProgress();
                            JSONObject object = new JSONObject(String.valueOf(response.body()));
                            if (object.getString("Status").equalsIgnoreCase("Success")) {
                                imageModelArrayList = new ArrayList<>();

                                List<BannerObjects> bannerObjects = new ArrayList<>();
                                JSONArray jsonArray = object.getJSONArray("Results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject obj = jsonArray.getJSONObject(i);
                                    if (obj.has("newslist")) {
                                        BannerObjects banObject = new BannerObjects();
                                        JSONArray array1 = obj.getJSONArray("newslist");
                                        for (int j = 0; j < array1.length(); j++) {
                                            JSONObject jsonObject = array1.getJSONObject(j);
                                            banObject.setId(jsonObject.getString("id"));
                                            banObject.setImage(jsonObject.getString("image"));
                                            banObject.setType("News");
                                            banObject.setTitle(jsonObject.getString("title"));
                                            banObject.setContent(jsonObject.getString("content"));
                                            banObject.setDate(jsonObject.getString("date"));
                                            bannerObjects.add(banObject);
                                            newsDescription = jsonObject.getString("content");
                                            newsTitle = jsonObject.getString("title");
                                            imageModelArrayList.add(banObject);
                                        }

                                    } else if (obj.has("project")) {

                                        BannerObjects banObject = new BannerObjects();
                                        JSONArray array1 = obj.getJSONArray("project");
                                        for (int j = 0; j < array1.length(); j++) {
                                            JSONObject jsonObject = array1.getJSONObject(j);
                                            banObject.setId(jsonObject.getString("id"));
                                            banObject.setImage(jsonObject.getString("Image"));
                                            banObject.setType("Project");
                                            banObject.setLocation(jsonObject.getString("venue"));
                                            banObject.setTitle(jsonObject.getString("name"));
                                            bannerObjects.add(banObject);
                                            imageModelArrayList.add(banObject);
                                        }
                                    } else if (obj.has("event")) {
                                        BannerObjects banObject = new BannerObjects();
                                        JSONArray array1 = obj.getJSONArray("event");
                                        for (int j = 0; j < array1.length(); j++) {
                                            JSONObject jsonObject = array1.getJSONObject(j);
                                            banObject.setId(jsonObject.getString("id"));
                                            banObject.setImage(jsonObject.getString("image"));
                                            banObject.setType("Event");
                                            banObject.setLocation(jsonObject.getString("venue"));
                                            banObject.setTitle(jsonObject.getString("name"));
                                            bannerObjects.add(banObject);
                                            imageModelArrayList.add(banObject);
                                        }

                                    }
                                }
                                mnewsTitle.setText(newsTitle);
                                mnewsDesc.setText(newsDescription);
                                sliderAdapter = new SliderAdapter(getActivity(), imageModelArrayList);
                                mPager.setAdapter(sliderAdapter);
                                sliderAdapter.setOnClickListen(new SliderAdapter.AddTouchListener() {
                                    @Override
                                    public void onTouchClick(int position) {
                                        String type = imageModelArrayList.get(position).getType();
                                        if (type.equalsIgnoreCase("newslist")) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("id", imageModelArrayList.get(position).getId());
                                            bundle.putString("title", imageModelArrayList.get(position).getTitle());
                                            bundle.putString("description", imageModelArrayList.get(position).getContent());
                                            bundle.putString("post", AppPreferences.getProfile(getContext()).get(position).getResults().getFirstname());
                                            bundle.putString("image", imageModelArrayList.get(position).getImage());
                                            bundle.putString("date", imageModelArrayList.get(position).getDate());
                                            Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
                                            intent.putExtra("data", bundle);
                                            startActivity(intent);
                                        } else if (type.equalsIgnoreCase("event")) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("name", imageModelArrayList.get(position).getTitle());
                                            bundle.putString("eventId", imageModelArrayList.get(position).getId());
                                            bundle.putString("decription", imageModelArrayList.get(position).getContent());
                                            bundle.putString("date", imageModelArrayList.get(position).getDate());
                                            bundle.putString("location", imageModelArrayList.get(position).getLocation());
                                            bundle.putString("image", imageModelArrayList.get(position).getImage());
                                            bundle.putString("status", String.valueOf(imageModelArrayList.get(position).getStatus()));
                                            Intent intent = new Intent(getActivity(), EventDetailActivity.class);
                                            intent.putExtra("type", bundle);
                                            startActivity(intent);
                                        } else if (type.equalsIgnoreCase("project")) {
                                            Bundle bundle = new Bundle();
                                            bundle.putString("name", imageModelArrayList.get(position).getTitle());
                                            bundle.putString("eventid", String.valueOf(imageModelArrayList.get(position).getId()));
                                            bundle.putString("image", imageModelArrayList.get(position).getImage());
                                            bundle.putString("description", imageModelArrayList.get(position).getContent());
                                            bundle.putString("date", imageModelArrayList.get(position).getDate());
                                            bundle.putString("location", imageModelArrayList.get(position).getLocation());
                                            startActivity(new Intent(getActivity(), ProjectDetailActivity.class).putExtra("data", bundle));
                                        }
                                    }
                                });
                                indicator.setViewPager(mPager);
                                final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
                                indicator.setRadius(5 * density);

                                NUM_PAGES = imageModelArrayList.size();

                                // Auto start of viewpager
                                final Handler handler = new Handler();
                                final Runnable Update = new Runnable() {
                                    public void run() {
                                        if (currentPage == NUM_PAGES) {
                                            currentPage = 0;
                                        }
                                        mPager.setCurrentItem(currentPage++, true);
                                    }
                                };
                                Timer swipeTimer = new Timer();
                                swipeTimer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        handler.post(Update);
                                    }
                                }, 3000, 3000);

                                // Pager listener over indicator
                                indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                                    @Override
                                    public void onPageSelected(int position) {
                                        currentPage = position;

                                    }

                                    @Override
                                    public void onPageScrolled(int pos, float arg1, int arg2) {

                                    }

                                    @Override
                                    public void onPageScrollStateChanged(int pos) {

                                    }
                                });

                            } else {
                                hideProgress();
                                Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            // Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle("Welcome " + AppPreferences.getProfile(getActivity()).get(0).getResults().getFirstname());
    }
}

