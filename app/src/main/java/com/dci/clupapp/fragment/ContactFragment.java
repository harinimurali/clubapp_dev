package com.dci.clupapp.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.dci.clupapp.R;
import com.dci.clupapp.activity.LoginActivity;
import com.dci.clupapp.activity.MainActivity;
import com.dci.clupapp.activity.ProjectDetailActivity;
import com.dci.clupapp.adapter.ProjectlistAdapter;
import com.dci.clupapp.app.ClubApplication;
import com.dci.clupapp.models.CommonResponse;
import com.dci.clupapp.models.ProjectResponse;
import com.dci.clupapp.retrofit.ClubAPI;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;
import com.dci.clupapp.utils.UtilsDefault;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;
import static com.dci.clupapp.activity.BaseActivity.AlertPopupWindow;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactFragment extends BaseFragment {
    @BindView(R.id.contact_name)
    EditText mName;
    @BindView(R.id.contact_email)
    EditText mEmail;
    @BindView(R.id.contact_message)
    EditText mMessage;
    @BindView(R.id.contact_send)
    Button mSend;
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    boolean valid = false;

    @Inject
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editor;
    @Inject
    public ClubAPI clubAPI;
    CommonResponse commonResponse;
    private SharedPreferences fcmSharedPrefrences;


    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contact, container, false);

        ButterKnife.bind(view);

        ClubApplication.getContext().getComponent().inject(this);
        editor = sharedPreferences.edit();
        fcmSharedPrefrences = getActivity().getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        mSend = view.findViewById(R.id.contact_send);
        mEmail = view.findViewById(R.id.contact_email);
        mMessage = view.findViewById(R.id.contact_message);
        mName = view.findViewById(R.id.contact_name);
        mName.setFilters(new InputFilter[]{editTextRestrictLength(), editTextRestrictSmileys(), editTextRestrictSpecialCharacters()});
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    contactAPI();
                }
            }
        });

        return view;
    }

    public boolean validate() {
        String name = mName.getText().toString();
        String email = mEmail.getText().toString();
        String message = mMessage.getText().toString();

        if (name.length() == 0 || name.length() < 2) {
            mName.setError(getString(R.string.field_required));
            valid = false;
        } else if (!UtilsDefault.isValidEmailAddress(email) || email.length() < 0) {
            mEmail.setError(getString(R.string.valid_email));
            valid = false;
        } else if (message.length() == 0 || message.length() < 4) {
            mMessage.setError(getString(R.string.field_required));
            valid = false;
        } else {
            mName.setError(null);
            mMessage.setError(null);
            mEmail.setError(null);
            valid = true;
        }
        return valid;
    }

    public void contactAPI() {
        showProgress();
        String deviceid = sharedPreferences.getString(Constants.DEVICEID, "");
        String appversion = sharedPreferences.getString(Constants.APPVERSION, "");
        String userid = sharedPreferences.getString(Constants.VENDORID, "");
        String osversion = String.valueOf(sharedPreferences.getInt(Constants.DEVICEOS, 0));
        String fcm_key = fcmSharedPrefrences.getString(Constants.FCMTOKEN, "");
        String name = mName.getText().toString().trim();
        String mail = mEmail.getText().toString().trim();
        String msg = mMessage.getText().toString().trim();

        clubAPI.ContactUs(userid, name, mail, msg, fcm_key, deviceid, osversion, appversion).
                enqueue(new Callback<CommonResponse>() {
                    @Override
                    public void onResponse(Call<CommonResponse> call, Response<CommonResponse> response) {
                        try {
                            hideProgress();
                            if (response.body() != null) {
                                commonResponse = response.body();
                                if (commonResponse.getStatus().equals("Success")) {

                                    AlertPopupWindow(getActivity(), "Message Sent successfully");
                                   //Toast.makeText(getActivity(), "Message Sent successfully", Toast.LENGTH_LONG).show();
                                    mEmail.setText("");
                                    mMessage.setText("");
                                    mName.setText("");

                                } else {
                                    Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            hideProgress();
                            e.printStackTrace();
                            Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<CommonResponse> call, Throwable t) {
                        //  hideProgress();
                        Toast.makeText(getActivity(), getResources().getString(R.string.try_again), Toast.LENGTH_LONG).show();
                    }
                });

    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.title.setText(getActivity().getResources().getString(R.string.contact_us));
    }

}
