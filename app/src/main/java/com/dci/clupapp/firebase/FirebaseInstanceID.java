package com.dci.clupapp.firebase;

import android.content.SharedPreferences;
import android.util.Log;

import com.dci.clupapp.utils.Constants;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;


/**
 * Created by keerthana on 8/24/2018.
 */
public class FirebaseInstanceID extends FirebaseInstanceIdService {
    @Inject
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("fcmid", "onTokenRefresh: " + refreshedToken);
        editor.putString(Constants.FCMTOKEN, refreshedToken).commit();
//        fcmDeviceKey=refreshedToken;
        System.out.println("==========" + refreshedToken + "=======");
        // AppPreferences.setFCMToken(getApplicationContext(), refreshedToken);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.FCMKEYSHAREDPERFRENCES, MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }
}
