package com.dci.clupapp.adapter;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by keerthana on 12/7/2018.
 */

public class MyWebViewClient extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        return false;
    }
}