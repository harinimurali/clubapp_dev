package com.dci.clupapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.NewsResponse;
import com.dci.clupapp.models.NewsTodayDTO;
import com.dci.clupapp.models.NewsTrendingDTO;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.AppPreferences;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.dci.clupapp.fragment.BaseFragment.getTimeFormat;

/**
 * Created by keerthana on 12/3/2018.
 */


public class NewsTodayAdapter extends RecyclerView.Adapter<NewsTodayAdapter.MyViewHolder> {


    private List<NewsResponse.Results.TodayItem> newsList;
    public AddTouchListen addTouchListen;
    Context context;
    private ImageView mShare;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.desc)
        TextView desc;
        @BindView(R.id.category)
        TextView category;
        @BindView(R.id.time)
        TextView duration;
        @BindView(R.id.header)
        LinearLayout header;

        public MyViewHolder(View view) {
            super(view);

            //binding view
            ButterKnife.bind(this, view);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public NewsTodayAdapter(Context context,List<NewsResponse.Results.TodayItem> newsList) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_today_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        NewsResponse.Results.TodayItem list = newsList.get(position);
        holder.name.setText(AppPreferences.getProfile(context).get(0).getResults().getFirstname());
        holder.desc.setText(list.getContent());
        holder.category.setText(list.getTitle());
      /*  holder.duration.setText(list.getDuration());
        holder.category.setText(list.getCategory());*/
        holder.duration.setText(getTimeFormat(list.getUpdatedAt()));
        Picasso.get().load(Constants.ImageUrl + list.getImage()).placeholder(R.drawable.ic_place_holder).fit().into(holder.image);



        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}