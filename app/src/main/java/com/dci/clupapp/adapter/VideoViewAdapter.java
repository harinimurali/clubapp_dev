package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.VideoResponse;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by keerthana on 12/17/2018.
 */


public class VideoViewAdapter extends RecyclerView.Adapter<VideoViewAdapter.MyViewHolder> {

    private  TextView text;
    // private  FullscreenVideoLayout videoLayout;
    private List<VideoResponse.VideoResults.VideoDataItem> videoList=new ArrayList<>();
    private Context context;
    private AddTouchListen addTouchListen;



   // private String url="http://clubapp.dci.in/uploads/video/SampleVideo_720x480_1mb.mp4";

    public VideoViewAdapter(List<VideoResponse.VideoResults.VideoDataItem> videoRespons) {
        this.videoList = videoRespons;
        this.context =context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView views,status,time;
        private YouTubePlayerView youtube;
        private LinearLayout header;
        ImageView img;

        public TextView text,date;
        //   public JzvdStd jzvdStd;


        public MyViewHolder(View view) {
            super(view);
            // video=(FullscreenVideoLayout)view.findViewById(R.id.videoView);

            date=view.findViewById(R.id.date);
            time=view.findViewById(R.id.time);
            text=view.findViewById(R.id.text);
            youtube = (YouTubePlayerView) view.findViewById(R.id.youtubePlayer);

        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_view_item_view, parent, false);

        return new MyViewHolder(itemView);
    }
    public void setOnClickListen(AddTouchListen addTouchListen)
    {
        this.addTouchListen=addTouchListen;

    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final VideoResponse.VideoResults.VideoDataItem movie = videoList.get(position);

        holder.text.setText(movie.getTitle());
        holder.date.setText(movie.getUpdatedAt());
        String date= BaseFragment.getDateFormat(movie.getUpdatedAt());
        holder.date.setText(date);
        holder.time.setText(movie.getUpdatedAt());
        String time= BaseFragment.getTimeFormat(movie.getUpdatedAt());
        holder.time.setText(time);

        holder.youtube.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                     //   String videoId ="cN0BUyXiXS8";
                     String videoId = movie.getUrl();
                             //getYoutubeID(videoList.get(position).getUrl());
                        initializedYouTubePlayer.loadVideo(videoId, 0);
                    }
                });
            }
        }, true);
      /*  holder.youtube.initialize(new YouTubePlayerInitListener() {
            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer initializedYouTubePlayer) {
                initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                    @Override
                    public void onReady() {
                        // String videoId ="0NUr7p7Hknk";
                        String videoId = getYoutubeID(youtubeVideoList.get(position).getVideoUrl());
                        initializedYouTubePlayer.loadVideo(videoId, 0);
                    }
                });
            }
        }, true);
*/

    }
/*
    public static String getYoutubeID(String youtubeUrl) {

        if (TextUtils.isEmpty(youtubeUrl)) {
            return "";
        }
        String video_id = "";

        String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*//*
;
        CharSequence input = youtubeUrl;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            String groupIndex1 = matcher.group(7);
            if (groupIndex1 != null && groupIndex1.length() == 11)
                video_id = groupIndex1;
        }
        if (TextUtils.isEmpty(video_id)) {
            if (youtubeUrl.contains("youtu.be/")  ) {
                String spl = youtubeUrl.split("youtu.be/")[1];
                if (spl.contains("\\?")) {
                    video_id = spl.split("\\?")[0];
                }else {
                    video_id =spl;
                }
            }
        }

        return video_id;
    }
*/

    public static String getYoutubeID(String youtubeUrl) {

        if (TextUtils.isEmpty(youtubeUrl)) {
            return "";
        }
        String video_id = "";

        String expression = "^.*((youtu.be" + "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
        CharSequence input = youtubeUrl;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(input);
        if (matcher.matches()) {
            String groupIndex1 = matcher.group(7);
            if (groupIndex1 != null && groupIndex1.length() == 11)
                video_id = groupIndex1;
        }
        if (TextUtils.isEmpty(video_id)) {
            if (youtubeUrl.contains("youtu.be/")  ) {
                String spl = youtubeUrl.split("youtu.be/")[1];
                if (spl.contains("\\?")) {
                    video_id = spl.split("\\?")[0];
                }else {
                    video_id =spl;
                }
            }
        }

        return video_id;
    }


    public interface AddTouchListen{
        public void OnTouchClick(int position);

    }
    @Override
    public int getItemCount() {
        return videoList.size();
    }
}

