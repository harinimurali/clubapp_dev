package com.dci.clupapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.NewsResponse;
import com.dci.clupapp.models.NewsTrendingDTO;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keerthana on 12/3/2018.
 */

public class NewsTrendingAdapter extends RecyclerView.Adapter<NewsTrendingAdapter.MyViewHolder> {


    private List<NewsResponse.Results.TrendingItem> newsList;
    public AddTouchListen addTouchListen;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.mLayout)
        RelativeLayout mLayout;


        public MyViewHolder(View view) {
            super(view);

            //binding view
            ButterKnife.bind(this, view);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public NewsTrendingAdapter(List<NewsResponse.Results.TrendingItem> newsList) {
        this.newsList = newsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_trending_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        NewsResponse.Results.TrendingItem list = newsList.get(position);
        holder.title.setText(list.getTitle());
        Picasso.get().load(Constants.ImageUrl + list.getImage()).placeholder(R.drawable.ic_place_holder).fit().into(holder.image);
//        holder.image.setImageResource(Integer.parseInt(list.getImage()));


        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}