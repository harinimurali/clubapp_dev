package com.dci.clupapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.ChatMessageParams;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecentChatListAdapter extends BaseAdapter {

    public RecentChatListAdapter(Context context, List<ChatMessageParams> contactLists) {
        this.context = context;
        this.contactLists = contactLists;
    }

    Context context;
    List<ChatMessageParams> contactLists;


    @Override
    public int getCount() {
        return contactLists.size();
    }

    @Override
    public ChatMessageParams getItem(int position) {
        return contactLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.childlayout_friendlist, null);
        }

        TextView textName = (TextView) convertView.findViewById(R.id.text_friendname);
        TextView text_designation = (TextView) convertView.findViewById(R.id.text_friendposition);
        ImageView img_seen = convertView.findViewById(R.id.img_seen);
        ImageView img_profile = convertView.findViewById(R.id.img_profile);
        try {
            Picasso.get().load(Constants.ImageUrl + contactLists.get(position).getReceiverPhotoUrl()).placeholder(R.drawable.ic_user).into(img_profile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        textName.setText(contactLists.get(position).getToName());
        if (contactLists.get(position).getText() == null) {
            text_designation.setText("image");
        } else {
            text_designation.setText(contactLists.get(position).getText());
        }
        if (contactLists.get(position).getReadStatus() == 0) {
            img_seen.setVisibility(View.VISIBLE);
            textName.setTextColor(context.getResources().getColor(R.color.black));
            text_designation.setTextColor(context.getResources().getColor(R.color.black));

        } else {
            img_seen.setVisibility(View.GONE);
            textName.setTextColor(context.getResources().getColor(R.color.hint_grey));
            text_designation.setTextColor(context.getResources().getColor(R.color.hint_grey));
        }


        return convertView;
    }


}
