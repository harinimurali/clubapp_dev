package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.ChatMessageModel;
import com.dci.clupapp.utils.Constants;
import com.dci.clupapp.utils.OnItemClickListener;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ViewHolder>  {

    private List<ChatMessageModel.Result> users;
    private Context context;
    private OnItemClickListener listener;

    public ContactListAdapter(List<ChatMessageModel.Result> users, Context context) {
        this.users = users;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.childlayout_friendlist, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int position) {
        ChatMessageModel.Result user = users.get(position);
        viewHolder.text_friendname.setText(user.getFirstname());
        viewHolder.text_friendposition.setText("president");
        viewHolder.relative_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnItemClick(position,view);
            }
        });
        try {
            Picasso.get().load(Constants.ImageUrl + user.getProfile_image()).placeholder(R.drawable.ic_user).into(viewHolder.img_profile);
        }catch (Exception e){
            e.printStackTrace();

        }


    }

    public void setItemclickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        return users.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView text_friendname,text_friendposition;
        public RelativeLayout relative_parent;
        CircleImageView img_profile;

        public ViewHolder(View view) {
            super(view);
            text_friendname = view.findViewById(R.id.text_friendname);
            text_friendposition = view.findViewById(R.id.text_friendposition);
            relative_parent = view.findViewById(R.id.relative_parent);
            img_profile = view.findViewById(R.id.img_profile);

        }
    }
}
