package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.dci.clupapp.R;
import com.dci.clupapp.models.GalleryDTO;
import com.dci.clupapp.models.GalleryDetailDTO;
import com.dci.clupapp.models.GalleryResponse;
import com.dci.clupapp.utils.Constants;
import com.joooonho.SelectableRoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by keerthana on 11/16/2018.
 */


public class GalleryDetailAdapter extends RecyclerView.Adapter<GalleryDetailAdapter.MyViewHolder> {

    // private  FullscreenVideoLayout videoLayout;
    private List<String> galList;
    private Context context;
    boolean isImageFitToScreen;
    private AddTouchListen addTouchListen;

    public GalleryDetailAdapter(List<String> galList) {
        this.galList = galList;
        this.context = context;
    }


    //    private String url="http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout header;
        SelectableRoundedImageView img;
        public VideoView v;


        public MyViewHolder(View view) {
            super(view);
            img = (SelectableRoundedImageView) view.findViewById(R.id.image);
            header = (LinearLayout) view.findViewById(R.id.header);
          /*  videoLayout = (FullscreenVideoLayout)view.findViewById(R.id.videoView);
            videoLayout.setActivity((Activity) context);
*/
        }
    }


  /*  public GalleryDetailAdapter(List<GalleryResponse.GalleryResult.GalleryAttachment> galList, Context context) {
        this.galList = galList;
        this.context = context;
    }*/

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_detail_item, parent, false);

        return new MyViewHolder(itemView);
    }

    public void setOnClickListen(AddTouchListen addTouchListen)

    {
        this.addTouchListen = addTouchListen;

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
       // GalleryResponse.GalleryResult.GalleryAttachment movie = galList.get(position);
        Picasso.get().load(galList.get(position)).fit().placeholder(R.drawable.ic_place_holder).into(holder.img);

      /*  Uri videoUri = Uri.parse("http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4");
        holder.v.setVideoURI(videoUri);*/

        holder.header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        if(isImageFitToScreen) {
                            isImageFitToScreen=false;
                            holder.img.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            holder.img.setAdjustViewBounds(true);
                        }else{
                            isImageFitToScreen=true;
                            holder.img.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                            holder.img.setScaleType(ImageView.ScaleType.FIT_XY);
                        }
                if (addTouchListen != null) {
                    addTouchListen.OnTouchClick(position);
                }
            }
        });
    }

    public interface AddTouchListen {
        public void OnTouchClick(int position);

    }

    @Override
    public int getItemCount() {
        return galList.size();
    }
}
