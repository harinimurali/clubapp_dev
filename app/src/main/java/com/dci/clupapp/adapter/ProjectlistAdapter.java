package com.dci.clupapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.MembersDTO;
import com.dci.clupapp.models.ProfileResponse;
import com.dci.clupapp.models.ProjectDTO;
import com.dci.clupapp.models.ProjectResponse;
import com.dci.clupapp.utils.AddTouchListen;
import com.dci.clupapp.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by harini on 11/19/2018.
 */

public class ProjectlistAdapter extends RecyclerView.Adapter<ProjectlistAdapter.MyViewHolder> {


    private List<ProjectResponse.DataRes> projectlist;
    public AddTouchListen addTouchListen;
    Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.project_title)
        TextView mTitle;
        @BindView(R.id.project_description)
        TextView mDescription;
        @BindView(R.id.project_image)
        ImageView mImage;
        @BindView(R.id.date)
        TextView mDate;
        @BindView(R.id.project_location)
        TextView mLocation;
        @BindView(R.id.project_layout)
        LinearLayout mlayout;

        public MyViewHolder(View view) {
            super(view);

            //binding view
            ButterKnife.bind(this, view);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public ProjectlistAdapter(List<ProjectResponse.DataRes> projectlist, Context context) {
        this.projectlist = projectlist;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.project_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        ProjectResponse.DataRes list = projectlist.get(position);
        holder.mTitle.setText(list.getName());
        holder.mDescription.setText(list.getDescription());
        Log.e("image", "description" + Constants.ImageUrl + list.getImage());
        Picasso.get().load(Constants.ImageUrl + list.getImage()).fit().into(holder.mImage);
        // holder.mImage.setImageResource(Integer.parseInt(list.getImage()));

        String date = BaseFragment.getDateFormat(list.getUpdatedAt());
        holder.mDate.setText(date);
        holder.mLocation.setText(list.getVenue());

        holder.mlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return projectlist.size();
    }
}