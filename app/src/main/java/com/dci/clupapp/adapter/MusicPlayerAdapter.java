package com.dci.clupapp.adapter;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.dci.clupapp.R;
import com.dci.clupapp.fragment.BaseFragment;
import com.dci.clupapp.models.MusicResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keerthana on 12/4/2018.
 */


public class MusicPlayerAdapter extends RecyclerView.Adapter<MusicPlayerAdapter.MyViewHolder> implements  Runnable {


   private  Context context;
    private List<MusicResponse.MusicResults> musicList;


    public AddTouchListen addTouchListen;
    MediaPlayer mediaplayer;
    private android.net.Uri Uri;
    private View view;

    MediaPlayer mediaPlayer = new MediaPlayer();
    SeekBar seekBar;
    boolean wasPlaying = false;
    private FloatingActionButton play;
    private TextView seekBarHint;
    private int progress;
    private boolean fromTouch;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.profile)
        ImageView profile;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.rel)
        RelativeLayout rel;



        public MyViewHolder(View view) {
            super(view);

            //binding view
            ButterKnife.bind(this, view);

        }
    }

    public void setOnClickListen(AddTouchListen addTouchListen) {
        this.addTouchListen = addTouchListen;
    }

    public MusicPlayerAdapter(List<MusicResponse.MusicResults> musicList) {
        this.musicList = musicList;
        this.context=context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.music_player_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final MusicResponse.MusicResults list = musicList.get(position);
//        holder.name.setText(list.getName());
//        holder.time.setText(list.getTime());
//        holder.title.setText(list.getTitle());
//        holder.profile.setImageResource(Integer.parseInt(list.getProfile()));


        holder.name.setText(list.getData().get(position).getTitle());
        holder.time.setText(list.getData().get(position).getUpdatedAt());
        String time = BaseFragment.getDateFormat(list.getData().get(position).getUpdatedAt());
        holder.time.setText(time);

        holder.rel.setOnClickListener(new View.OnClickListener() {



         // public   String url="https://www.android-examples.com/wp-content/uploads/2016/04/Thunder-rumble.mp3";

            @Override
            public void onClick(View v) {
                if (addTouchListen != null) {
                    addTouchListen.onTouchClick(position);
                }

            }

        });


    }



    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
        seekBarHint.setVisibility(View.VISIBLE);
        int x = (int) Math.ceil(progress / 1000f);

        if (x < 10)
            seekBarHint.setText("0:0" + x);
        else
            seekBarHint.setText("0:" + x);

        double percent = progress / (double) seekBar.getMax();
        int offset = seekBar.getThumbOffset();
        int seekWidth = seekBar.getWidth();
        int val = (int) Math.round(percent * (seekWidth - 2 * offset));
        int labelWidth = seekBarHint.getWidth();
        seekBarHint.setX(offset + seekBar.getX() + val
                - Math.round(percent * offset)
                - Math.round(percent * labelWidth / 2));

        if (progress > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
            clearMediaPlayer();
            play.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_play));
            this.seekBar.setProgress(0);
        }

    }


    public void onStopTrackingTouch(SeekBar seekBar) {


        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.seekTo(seekBar.getProgress());
        }
    }



    public void playSong() {

        try {


            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                clearMediaPlayer();
                seekBar.setProgress(0);
                wasPlaying = true;
                play.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_play));
            }


            if (!wasPlaying) {

                if (mediaPlayer == null) {
                    mediaPlayer = new MediaPlayer();
                }

                play.setImageDrawable(ContextCompat.getDrawable(context, android.R.drawable.ic_media_pause));

             AssetFileDescriptor descriptor = context.getAssets().openFd("Thunderrumble.mp3");
                mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
                descriptor.close();

                mediaPlayer.prepare();
                mediaPlayer.setVolume(0.5f, 0.5f);
                mediaPlayer.setLooping(false);
                seekBar.setMax(mediaPlayer.getDuration());

                mediaPlayer.start();
                new Thread(this).start();

            }

            wasPlaying = false;
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();

        onProgressChanged( seekBar,  progress,  fromTouch);

        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }

            seekBar.setProgress(currentPosition);

        }
    }


    protected void onDestroy() {
       // super.onDestroy();
        clearMediaPlayer();
    }

    private void clearMediaPlayer() {
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }


    @Override
    public int getItemCount() {
        return musicList.size();
    }
    public interface AddTouchListen {
        public void onTouchClick(int position);
    }
}